# TransTrust Credential
TransTrust Credential是具体的文档凭证实例，所有Credential都需要满足对应CPT所设计的JSON Schema。Credential主要由可信凭证和可流转凭证两种类型，分别对应可信凭证管理合约和流转管理合约，在CPT创建时就确定所有依据该CPT签发的文档凭证时那种类型以及对应的管理合约地址。

- 可信凭证：用于证明某个用户拥有某个文档或证件，并且包含该文档或证件的详细信息。一个可信凭证只能由原持有者使用。在具体使用场景中，通常由用户出示可信凭证给第三方，第三方可以验证可信凭证的完整性和无篡改。可信凭证的摘要信息（比如Credential的ID和哈希）存储在对应的可信凭证管理合约中，完整凭证内容则加密存储在链下的数据存储中心。
- 可流转凭证：满足可验证性，同时可以在不同用户之间流转。在流转管理合约中，使用非同质化Token记录每个可流转凭证的摘要信息，每签发一个可流转凭证都会在流转管理合约中生成一个非同质化Token来唯一代表这个可流转凭证。此外流转管理合约中还记录着每个非同质化Token的拥有者和持有者的信息，经过用户商议和流转后，在链上的流转管理合约中改变非同质化Token拥有者或持有者的信息，而链下对应的凭证则转移到新的拥有者或持有者。只有在链上的流转管理合约中所记录的当前拥有者或持有者才能使用非同质化Token对应的可流转凭证。

## TransTrust Credential规范

TransTrust Credential主要包含四个部分的内容：CPT Information、Credential Properties和Proof，其中Credential Information又可分为Credential MetaData和Credential Claim。

<img src="../../images/image-20211117110506611.png" alt="image-20211117110506611" style="zoom:80%;" />

- CPT Information：Credential对应的CPT的完整信息，除了原来的Credential Properties部分。
- Context：所有Credential的固定字段，标识了所使用的TransTrust系统版本，当前默认为https://github.com/WeBankFinTech/WeIdentity/blob/master/context/v1。
- Credential Properties：Credential相关的字段，包含以下两个部分内容：
  - Credential MetaData：Credential的元数据，包括签发者和接收者的信息、Credential的签发和到期时间和Credential的序列号等。
  - Credential Claim：Credential的主要内容，具体业务属性字段。
- Proof：Credential相关的证明，包括计算Claim中的字段的哈希所用的盐和签发者的签名等。

CPT Information和Credential Properties中各个字段的注释在TransTrust CPT规范中已经详细介绍，这里主要介绍和Credential相关的字段，包括Credential Properties（在原来CPT中的Credential Properties基础上增加了docType字段，隐藏required字段）和Proof。从整体格式上，一个Credential实例仅仅比对应的CPT多了Proof和context字段。
| 类别 | 属性 | 注释 |
| -- | ----- | ---- |
| Context | context | Credential的TransTrust系统版本信息 |
| Credential Properties | properties | Credential相关字段域 |
| Credential Properties | properties.id | Credential的序列号 |
| Credential Properties | properties.issuanceDate | Credential的签发日期 |
| Credential Properties | properties.expirationDate | Credential的到期日期 |
| Credential Properties | properties.recipient | Credential的接收者信息，包含WeID和名字 |
| Credential Properties | properties.issuer | Credential的签发者信息，包含WeID和名字 |
| Credential Properties | docType | Credential的类型，默认是VerifiableCredential和original |
| Credential Properties | properties.claim{} | Credential的具体业务属性字段，需要Issuer按照$schema中规定的格式定义各个字段 |
| Proof | proof | Credential相关的证明 |
| Proof | proof.created | Credential的签发时间 |
| Proof | proof.creator | Credential的签发者 |
| Proof | proof.salt{} | Claim中的字段的哈希所用的盐，与Claim中的字段一一对应 |
| Proof | proof.signatureValue | 签发者对整个Credential的签名 |
| Proof | proof.type | 签名的类型，目前仅支持国密的SM3和非国密的Secp256k1 |

同样以土地证为例，各个市的土地管理局根据土地证CPT签发土地证Credential，只需要填写土地证CPT的properties.claim{}中各个字段的内容（不是required的可以不用填写）、接收者的信息和Credential的到期时间，系统自动补全其他信息和生成Proof。因此，所有的土地证Credential都有相同的CPT Information，而Credential Properties和Proof部分则不同。具体可以参考土地证Credential实例。

## Credential生命周期管理

Credential的生命周期仅有签发创建和撤销，为了保证文档的可信，文档签发者需要对文档内容签名，因此文档不能更新。如果需要改变文档内容，只能另外签发一个Credential。
Authority Issuer创建了一个授权CPT后，还需要部署该CPT的权限管理合约、存证管理合约或流转管理合约，搭建或使用推荐的Credential加密存储服务（或者复用已有的）。这些合约和服务用于Credential的生命周期。这里以根据某个授权CPT签发的Credential的生命周期管理为例。假设Authority Issuer已经将所有可以使用该CPT来签发Credential的签发者（WeID）添加到对应的权限管理合约中。

### 1.	Credential的签发

1. 用户向有签发权限的签发者申请一个Credential，并提供相关信息如WeID document。
2. 签发者验证用户信息（包括在权限管理合约中查询该用户的WeID是否在白名单或黑名单中）后，根据业务相关原始文件（可无）填写CPT中各个字段的内容并签名，系统后台自动解析校验，生成签名凭证SignCredential（默认全不披露，即所有字段都单独计算哈希，template字段全部加盐）并发给用户。如果有多个签发者，需要每个签发者分别添加签名，把完全签名凭证SignCredential发给用户（目前还没实现多签的接口）。
3. 签发者对完全签名凭证SignCredential计算总hash，将总hash和template中各字段的hash提交到CPT的存证管理合约或流转管理合约，形成链上的可信文档或者可流转文档。同时将签名凭证SignCredential提交到链下存储服务中加密存储。
4. 用户收到签名凭证SignCredential后，可以验证签名凭证SignCredential的正确性、在存证管理合约或流转管理合约中查询以及执行流转等操作。
5. 如果用户需要向某验证方提供选择性披露的签名凭证SignCredential，可以自行删除签名凭证SignCredential中的某些明文内容和盐，仅保留哈希值；也可以将签名凭证SignCredential和选择性披露策略提供给签发者，经过类似操作后返回标准的选择性披露凭证SelectiveCredential，能够同时满足凭证本身的验证和链上可信文档或可流转文档的验证。

### 2.	Credential的撤销

1. 用户向对应签发者申请撤销指定的Credential，并提供相关信息如WeID documen和签名凭证SignCredential。
2. 签发者验证用户信息（包括该用户是否是签名凭证SignCredential的拥有者或者在流转管理合约中是否拥有撤销的权限），在存证管理合约或流转管理合约中删除相关记录或者将其状态标识为作废。

>**根据存证管理合约中设计Credential的各类状态，除了撤销外，还可以有冻结和恢复等操作，其过程与撤销类似。签发者拥有主动撤销、冻结和恢复其所签发的Credential的权限。**

## TransTrust Credential和各个角色之间的关系

![roles-relation](../../images/roles-relation.png)

在WeIdentity生态中，存在着以下角色：
| 角色 | 说明 |
| -- | ----- |
| User (Entity) | 用户（实体）。会注册属于自己的WeIdentity DID，申请Credential，并通过出示给相关业务方来使用之。 |
| Issuer | Credential的发行者。会首先验证实体对WeIdentity DID的所有权，其次发行Credential交给实体。 |
| Verifier | Credential的使用者。会首先验证实体对WeIdentity DID的所有权，其次验证Credential的有效性。 |
| User Agent / Credential Repository | 用户（实体）在此生成WeIdentity DID。为了便于使用，实体也可将自己的私钥、持有的Credential托管于此。 |

场景：

![scenario](../../images/scenario.png)

上图展示了五个WeIdentity生态下Credential在不同角色间流转的场景：

1. 身份证明机构作为Issuer向用户发行「实名认证Credential」，政府机构作为Verifier在办理公共事务时对其进行验证。
2. 学校作为Issuer向用户发行「学历证明Credential」，公司作为Verifier在对候选人进行背景调查时对其进行验证。
3. 出入境机构作为Issuer向用户发行「签证Credential」，海关作为Verifier在出入境时对其进行验证。
4. 公司作为Issuer向用户发行「收入证明Credential」，银行作为Verifier在发放贷款时对其进行验证。
5. 商户作为Issuer向用户发行「优惠券Credential」，商户自己作为Verifier在对优惠券核销时对其进行验证。

## TransTrust Credential的选择性披露

详情查看WeIdentity的选择性披露设计[WeIdentity 规范 — WeIdentity 文档](https://weidentity.readthedocs.io/zh_CN/latest/docs/weidentity-spec.html#id17)



