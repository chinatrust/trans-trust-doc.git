# TransTrust CPT
Claim Protocol Type（CPT）描述了一个TransTrust中各类文档凭证所依据的模板样式，类似各种空白业务表或文件，包含了所有必备的内容，依据某个CPT签发文档凭证即是在对应的空白业务表或文件的指定处填写内容。

TransTrust CPT以JSON作为基础格式，使用JSON Schema来定义TransTrust文档的数据模型标准。TransTrust中的文档（可信文档和可流转文档）都采用相同的数据模型标准。在TransTrust的基础模板框架上，不同的Issuer按业务场景需要，各自创建不同的CPT，主要是定义CPT中文档凭证相关的字段和格式。一个具体业务场景的CPT，定义了该业务所需签发的文档凭证的必要和可选字段，所有依据该CPT签发的文档凭证必须满足该CPT的格式要求。

## TransTrust CPT规范

TransTrust CPT包含五个方面内容：CPT Fixed Information，CPT Base Information，CPT Properties，Credential Properties和CPT MetaData。

- CPT Fixed Information：CPT的固定信息，描述了所有CPT都遵循的统一格式规范。
- CPT Base Information：CPT的基础信息，包含CPT的序号和版本号，在注册CPT时，系统自动填写。
- CPT Properties：TransTrust规定的所有CPT必须包含的属性，用于介绍、展示和管理CPT，需要Issuer在创建CPT时填写每个属性的内容。
- Credential Properties：文档凭证相关字段域，需要Issuer在创建CPT时按照$schema中规定的格式定义业务属性字段和必选字段要求。
- CPT MetaData：CPT的元数据，包含Issuer的信息和签名等，在注册CPT时，系统自动填写。

![](../../images/cpt-structure.png)

CPT各属性含义：

| 类别 | 属性 | 注释 |
| -- | ----- | ---- |
| CPT Fixed Information | $schema | CPT满足的的json格式，固定为http://json-schema.org/draft-04/schema# |
| CPT Fixed Information | Type | 整个CPT的类型，固定为object |
| CPT Base Information | cptId | CPT的ID |
| CPT Base Information | cptVersion | CPT的版本 |
| CPT Properties | cptType | CPT类型（ORIGINAL或者ZKP，默认ORIGINAL） |
| CPT Properties | cptName | CPT名字 |
| CPT Properties | cptDescription | CPT简介 |
| CPT Properties | cptManagment | 权限管理合约地址（Address），管理所有可以使用该CPT签发文件的WeID和可以申请文件的用户WeID（白名单）或不可以申请文件的用户WeID（黑名单） |
| CPT Properties | credentialType | 文档凭证类型（可信文档凭证/可流转文档凭证），包括对应的存证管理合约地址或流转管理合约地址 |
| CPT Properties | url | 查看或申请基于该CPT签发的文档凭证的网址 |
| CPT Properties | supervisable | 是否支持监管（true/false） |
| Credential Properties | properties | 文档凭证相关字段域，这些字段也会作为CPT的内容保存到合约中并根据这些字段对文档凭证做检验 |
| Credential Properties | properties.id | 文档凭证的序列号 |
| Credential Properties | properties.issuanceDate | 文档凭证的签发日期 |
| Credential Properties | properties.expirationDate | 文档凭证的到期日期 |
| Credential Properties | properties.recipient | 文档凭证的接收者信息，包含WeID和名字 |
| Credential Properties | properties.issuer | 文档凭证的签发者信息，包含WeID和名字 |
| Credential Properties | properties.claim{} | 文档凭证的具体业务属性字段，需要Issuer按照$schema中规定的格式定义各个字段 |
| Credential Properties | required | 文档凭证必须填写的properties.claim中的字段 |
| CPT MetaData | cptPublisher | CPT创建者的信息，包含WeID |
| CPT MetaData | cptSignature | 创建者对CPT内容的签名 |
| CPT MetaData | created | CPT创建时间 |
| CPT MetaData | updated | CPT更新时间 |

以土地证为例，国家土地管理部门需要指定统一的土地证样式，即土地证CPT，需要填写CPT Properties里各个属性的内容，然后在Credential Properties的claim中定义土地证所包含的字段以及明确哪些字段是必填的。

## TransTrust CPT与链上智能合约的关系

区块链上有三个智能合约与CPT联系紧密，分别为CPT创建管理合约、CPT数据存储合约和CPT权限管理合约，通过TransTrust SDK中相关的CPT服务接口可以和这个三个智能合约交互，实现注册和更新CPT、获取CPT内容和CPT权限管理等功能。

![](../../images/image-20211116160406530.png)

其中CPT创建管理合约和CPT数据存储合约由系统管理员在系统初始化时部署到区块链上，作为全局服务管理和存储所有的CPT。所有的CPT都需要向CPT创建管理合约注册，获得唯一的CPT ID。TransTrust规定了两种CPT：一种是系统CPT（包括WeID的系统CPT），用于定义一些通用的凭证，由系统管理员创建和注册，分配0-1000的CPT ID；另外一种是授权CPT，用于定义不同业务场景的文档凭证，由各个Authority Issuer创建和注册，分配1000以上的CPT ID。注册成功后，CPT的内容放到CPT数据存储合约存储，而CPT创建管理合约仅存储CPT的简要信息。

CPT支持更新，可更新的内容包括CPT Properties和Credential Properties中的claim部分，因此需要Issuer重新填写CPT Properties里各个属性的内容、在Credential Properties的claim中定义新的业务字段格式和新的必填字段，系统自动增加该CPT的版本号、改变更新时间和签名，然后和新的CPT内容一起更新到链上的CPT数据存储智能合约，这个过程也要先经过CPT创建管理合约的权限审查，即更新的Issuer是不是这个CPT的创建Issuer。

![](../../images/image-20211116161707335.png)

CPT权限管理合约则是每个CPT独自拥有一个，由对应的Issuer在创建CPT前部署到区块链上，并且将合约地址填写到CPT的cptManagment字段中。CPT权限管理合约管理所有可以使用该CPT签发文件的WeID和可以申请文件的用户WeID（白名单）或不可以申请文件的用户WeID（黑名单），以及提供其他的权限管理功能。

所有实现具体可参考CPT相关接口[**TransTrust CPT SDK**](../Components/javasdk/index.md)。

## TransTrust CPT基础模板

下面这个json对象是TransTrust文档的定义，所有的CPT都要满足该格式，具体CPT示例可以参考LandCertificateCPT。
```json
{
    "$schema" : "http://json-schema.org/draft-04/schema#",
    "type" : "object",
    "cptType" : {
        "type" : "string",
        "description" : "the Type of this CPT",
        "enum" : ["ORIGINAL","ZKP"],
        "default" : "ORIGINAL"
    },
    "cptId" : {
        "type" : "integer",
        "description" : "the ID of this CPT"
    },
    "cptVersion" : {
        "type" : "integer",
        "description" : "the version of this CPT"
    },
    "cptName" : {
        "type" : "string",
        "description" : "the name of this CPT"
    },
    "cptDescription" : {
        "type" : "string",
        "description" : "the description of this CPT"
    },
    "cptManagment" : {
        "type" : "string",
        "pattern" : "^0x[a-fA-F0-9]{40}$",
        "description" : "the address of CPT authority management contract"
    },
    "credentialType" : {
        "type" : "object",
        "description" : "whether the docs based on this CPT are trustful credentials or tradeful credentials",
        "oneof" : [
            {
                "cptEvidenceStore" : {
                    "type" : "string",
                    "pattern" : "^0x[a-fA-F0-9]{40}$",
                    "description" : "the address of credential evidences store contract based on this CPT"
                }
            },
            {
                "cptTradeToken" : {
                    "type" : "string",
                    "pattern" : "^0x[a-fA-F0-9]{40}$",
                    "description" : "the address of ERC721 contract based on this CPT"
                }
            }
        ]
    },
    "url" : {
        "type" : "string",
        "description" : "website url that users can apply, read or issue a credential based on this CPT"
    },
    "supervisable" : {
        "type" : "string",
        "description" : "the type of this CPT",
        "enum" : ["false","true"]
    },
    "properties" : {
        "id" : {
            "type" : "string",
            "description" : "the ID or serial number of a credential"
        },
        "claim" : {},
        "issuanceDate" : {
            "type" : "long",
            "description" : "the creating date of a credential"
        },
        "expirationDate" : {
            "type" : "long",
            "description" : "the expiration date of a credential"
        },
        "recipient" : {
            "type" : "object",
            "description" : "the information of credential recipient",
            "properties" : {
                "WeID" : {
                    "type" : "string",
                    "description" : "the WeID of credential recipient"
                },
                "name" : {
                    "type" : "string",
                    "description" : "the name of credential recipient"
                }  
            },
            "required" : ["WeID"]
        },
        "issuer" : {
            "type" : "object",
            "description" : "the information of credential issuer",
            "properties" : {
                "WeID" : {
                    "type" : "string",
                    "description" : "the WeID of credential issuer"
                },
                "name" : {
                    "type" : "string",
                    "description" : "the name of credential issuer"
                }  
            },
            "required" : ["WeID"]
        }
    },
    "cptPublisher" : {
        "type" : "string",
        "description" : "the description of this CPT"
    },
    "cptSignature" : {
        "type" : "string",
        "description" : "the description of this CPT"
    },
    "created" : {
        "type" : "string",
        "description" : "the creating date of this CPT"
    },
    "updated" : {
        "type" : "string",
        "description" : "the updating date of this CPT"
    }
}
```

