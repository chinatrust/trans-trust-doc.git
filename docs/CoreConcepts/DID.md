# 分布式身份（DID）

分布式身份在TransTrust系统中主要作用是实现实体对象（人或者物）的现实身份与链上身份的可信映射。文档发行者，文档的持有者，文档的验证者都可以持有一个DID。

这些角色都持有一个自己持有控制DID的私钥，用签名来行使自己角色的权利。

TransTrust的DID方案采用了由微众银行自主研发的分布式多中心的分布式身份技术解决方案[WeIdentity](https://weidentity.readthedocs.io/zh_CN/latest/)。
