# 使用场景

TransTrust 体系包含了区块链身份DID、文档凭证Credential和流转管理等功能，具有非常丰富的使用场景。在当前数字化社会，绝大部分文档凭证的生成方式都是在一个设计好的凭证模板上填充内容，然后导出或者打印得到一个完整的可用凭证，比如电子发票，商家在全国统一的发票模板上填写必要的内容，然后导出得到一个实际的电子发票凭证；还有使用更广泛的超市购物清单，同样是在该超市的清单模板上填写货物和价格等信息后，然后导出和打印得到一个实际的清单凭证。但是这些传统的电子或纸质文档凭证的验证通常需要通过各自的不同接口和途径去验证其真实性，缺少一个统一的方式可以公开验证所有的文档凭证都没有被篡改。对于如何将一个普通的文档凭证转化成一个可信可公开验证的Credential，WeIdentity项目已经做了详细的描述以及展示了多个使用场景[使用场景 — WeIdentity 文档](https://weidentity.readthedocs.io/zh_CN/latest/docs/use-cases.html#weidentity)。

TransTrust 在WeIdentity的基础上增加了Credential的流转管理等功能，使得不仅可以保证Credential链下流转的可信性，还可以实际地在不同实体（WeID）之间流转，即所有权的流转，而前者只是在存储介质上的流转，极大地扩展了使用场景。可流转凭证结合TransTrust 的基于场景的隐私保护功能，使得Credential更加安全和实用，更加贴近纸质化的使用方式。相比于可信Credential，可流转的Credential通常被赋予了实质的价值。下面主要介绍可流转凭证的使用场景及使用方式。

## 场景一：跨境贸易信用证

- 背景

国外进口公司与国内出口公司签订了交易合同，但是双方之间缺乏信任基础，通常需要国外进口公司预付款或者全款，国内出口公司才委托物流公司把商品运送交给国外进口公司，但是又要保证国内出口公司正常发货以及物流环节没有欺骗，因此又需要第三方做中间担保，过程比较复杂。如果国外进口公司暂时没有足够的现金流支付，更进一步降低双方之间的信任。其中最重要的原因是缺乏可信的且可流转凭证，作为信用基石。

- 参与方：
  - 商务部
  - 国内出口公司
  - 国内银行
  - 跨国物流公司
  - 国外银行
  - 国外进口公司



- 解决方案及基本流程

  假设所有参与方都已经进行DID注册和KYC认证，为了完整地描述整个场景，从国内出口公司向商务部申请出口许可证开始，具体过程如下图：

  ![img](../../images/crossboard.png)

整个过程涉及两种可流转凭证Credential，分别是信用证Credential和货物提单Credential，出口许可证Credential不具备流转特性，因此只是一个可信Credential。在这个场景中，信用证Credential由国外银行签发，代表了国外银行对国外进口公司的付款信用做担保；货物提单Credential由跨国物流公司签发，代表了物流公司对这个Credential的所有者（链上的智能合约中记录的所有者，并非任意的持有这个Credential的人）都可以提取货物的承诺。这些Credential可以被公开地验证其正确性和查询当前实际所有者，不怕被偷窃和遗失，流转过程透明。随着者两个Credential的流转和所有者发生变更，Credential对应的实际资产的所有权也发生变更。

## 场景二：门票凭证

- 背景

目前很多门票（比如演唱会门票和景区门票等）都实现了线上购买和电子化票据，有些门票需要实名购买及验票（人与门票绑定），但是如果某个用户购买了门票但是临时有事无法观演或游玩，又无法转让门票（因为需要使用购买门票的人的身份证才能通过验票），因此造成一定的经济损失。目前更多的是无需实名购买的电子门票（人与门票不绑定），因此会有人（俗称黄牛）购买了很多张票再高价卖出，而且在转卖过程中经常出现欺诈行为，因为门票通常没有公开验证途径，甚至有些黄牛一票多卖。

- 参与方
  - 门票发行公司
  - 演出举办公司
  - 观演用户



- 解决方案及基本流程

1. 门票发行公司、演出举办公司和观演用户分别进行DID注册
2. 门票发行公司创建统一的门票凭证模板CPT（如果有特殊类型门票要求，可以在创建新的CPT）。
3. 演出公司（或授权门票发行公司）依据门票凭证模板CPT向购买门票的观演用户签发门票Credential，并且在区块链上的流转管理合约生成每个门票Credential对应的非同质化Token并记录其所有者信息。
4. 观演用户A转卖其拥有的门票Credential给观演用户B，仅需通过购票软件发送交易，将他的门票Credential对应的非同质化Token的所有者更换成观演用户B。
5. 观演用户B验证门票Credential是否已经成功流转后付款（第四和第五步可以视情况对换顺序）。
6. 观演用户B持门票Credential观演，演出举办公司可以验证这个Credential是否属于观演用户B，并且还没有被使用过。
7. 门票发行公司可以根据链上的交易历史信息统计所有Credential的流转行为，制定更好的买票方式。







