# 系统设计

## 概览
![](../../images/modes.jpg)

## 逻辑架构
![](../../images/logic.png)

## 多层治理模型
![](../../images/government.png)