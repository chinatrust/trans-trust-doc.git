# 数据服务组件

```eval_rst
.. toctree::
   :maxdepth: 1

   README.md
   install.md
   interface.md
   appendix.md
```
## 服务介绍

本服务是同步TransTrust数据，主要是Did数据、Cpt数据、Document数据等。其中：

1.initService服务：初始化服务，将区块链节点上的原始数据同步到数据库，默认以500区块，500个CPT方式循环同步；

2.updateChainDataTask定时任务：定时每秒增量更新区块链节点数据至数据库。
