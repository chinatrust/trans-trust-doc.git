# 接口文档

## 接口说明

### 1.分页查询Did接口

#### 接口描述 

分页查询Did信息

#### 接口Url

http://localhost:6021/query/did/page?page=1&size=10

#### 调用方法

HTTP GET

#### 请求参数

##### 1)参数表

| 参数     | 字段 | 举例                      |
| -------- | ---- | ------------------------- |
| **页面** | page | 1 (表示第2页)             |
| **类型** | size | 10 (表示每页显示数量为10) |

##### 2)数据格式



#### 响应参数

##### 1)数据格式

```
{
  "code": 0,
  "message": "success",
  "data": {
    "content": [
      {
        "id": 11,
        "did": "did:weid:1:0x8957186c9d27042b5c6108b488c1f924d2a985a4",
        "created": 1634648886,
        "currentBlockNum": 73,
        "previousBlockNum": 72,
        "accValue": null,
        "name": null,
        "isAuthority": "NO"
      },
      {
        "id": 12,
        "did": "did:weid:1:0x1b54c80b5c535eba19c40984af625eb0eca52186",
        "created": 1634649341,
        "currentBlockNum": 74,
        "previousBlockNum": 73,
        "accValue": null,
        "name": null,
        "isAuthority": "NO"
      },
      {
        "id": 13,
        "did": "did:weid:1:0x41454ac21e3575f71fd7286ccf0ab79534c58139",
        "created": 1634649604,
        "currentBlockNum": 75,
        "previousBlockNum": 74,
        "accValue": null,
        "name": null,
        "isAuthority": "NO"
      },
	   ......
      {
        "id": 20,
        "did": "did:weid:1:0x93fe57b84e488c89aad419bb7452625d54213ecd",
        "created": 1634711215,
        "currentBlockNum": 83,
        "previousBlockNum": 82,
        "accValue": null,
        "name": null,
        "isAuthority": "NO"
      }
    ],
    "pageable": {
      "sort": {
        "empty": true,
        "sorted": false,
        "unsorted": true
      },
      "offset": 10,
      "pageSize": 10,
      "pageNumber": 1,
      "paged": true,
      "unpaged": false
    },
    "last": false,
    "totalPages": 127,
    "totalElements": 1267,
    "number": 1,
    "size": 10,
    "sort": {
      "empty": true,
      "sorted": false,
      "unsorted": true
    },
    "numberOfElements": 10,
    "first": false,
    "empty": false
  },
  "attachment": null
}
```

### 2.分页查询Cpt

#### 接口描述 

分页查询Cpt信息

#### 接口Url

http://localhost:6021/query/cpt/page?page=0&size=5

#### 调用方法 

HTTP GET

#### 请求参数

##### 1)参数表

| 参数     | 字段 | 举例                    |
| -------- | ---- | ----------------------- |
| **页面** | page | 0 (表示第1页)           |
| **类型** | size | 5 (表示每页显示数量为5) |

##### 2)数据格式



#### 响应参数

##### 1)数据格式

```
{
  "code": 0,
  "message": "success",
  "data": {
    "content": [
      {
        "id": 1,
        "cptId": 11,
        "cptVersion": 1,
        "cptPublisher": "did:weid:1:0x8e5831ceb8451931a34b6d49a4bae8979d0b4041",
        "cptSignature": "AfWlduX6mcQZwhyyBSJgYf2wrDrGN11sCFaQSePDeqAYQE+nmH+rLI5EgNUQAVYxpCZBw/pzmw5gqf3EpdgPS6I=",
        "updated": 0,
        "created": 1633941876
      },
      {
        "id": 2,
        "cptId": 101,
        "cptVersion": 1,
        "cptPublisher": "did:weid:1:0x8e5831ceb8451931a34b6d49a4bae8979d0b4041",
        "cptSignature": "AElO/ruVHetaO0L98j5fx28VsYK/ej+K57oXWwb1+NwzJTMytkzbnk7FtVUCZPKIWO7w20tp3DANC6CkzN6YR00=",
        "updated": 0,
        "created": 1633941876
      },
      {
        "id": 3,
        "cptId": 103,
        "cptVersion": 1,
        "cptPublisher": "did:weid:1:0x8e5831ceb8451931a34b6d49a4bae8979d0b4041",
        "cptSignature": "ALUFlkOaszc3lRXCfBNFuK9mN/NuKVfNy4EFfjBfz7zKKB5NqGegXa81+N4CEM1058frDb4zuhRBSv3fXnU/JMQ=",
        "updated": 0,
        "created": 1633941877
      },
      {
        "id": 4,
        "cptId": 105,
        "cptVersion": 1,
        "cptPublisher": "did:weid:1:0x8e5831ceb8451931a34b6d49a4bae8979d0b4041",
        "cptSignature": "AFwDMhjq8lCnp+7hmbgf2BsdRbuSIQze8cn/6uvcd2EIFDiu7P6Q2sdByg2pZKCpPMQYz5JUMLAU2nICN4WiIXI=",
        "updated": 0,
        "created": 1633941877
      },
      {
        "id": 5,
        "cptId": 106,
        "cptVersion": 1,
        "cptPublisher": "did:weid:1:0x8e5831ceb8451931a34b6d49a4bae8979d0b4041",
        "cptSignature": "AJy7Q6GnK8VMNf7xXZDqV2cFuJZMo/CC8TntDv2RMZWoL43u+sQeKlzPGeJamVBjTb2GpiXE2ra9nYfv3Lbui4I=",
        "updated": 0,
        "created": 1633941878
      }
    ],
    "pageable": {
      "sort": {
        "empty": true,
        "sorted": false,
        "unsorted": true
      },
      "offset": 0,
      "pageSize": 5,
      "pageNumber": 0,
      "unpaged": false,
      "paged": true
    },
    "last": false,
    "totalElements": 16,
    "totalPages": 4,
    "number": 0,
    "size": 5,
    "sort": {
      "empty": true,
      "sorted": false,
      "unsorted": true
    },
    "numberOfElements": 5,
    "first": true,
    "empty": false
  },
  "attachment": null
}
```

### 3.查询Did详情

#### 接口描述

查询Did的详细信息

#### 接口Url

http://localhost:6021/query/did/detail?did=did%3Aweid%3A1%3A0xccf92a3b5754ecbd441b5b1fbcc3a9db12d4fbcb

#### 调用方法

HTTP GET

#### 请求参数

##### 1)参数表

| 参数 | 字段 | 举例                                                  |
| ---- | ---- | ----------------------------------------------------- |
| DID  | did  | did:weid:1:0xccf92a3b5754ecbd441b5b1fbcc3a9db12d4fbcb |

##### 2)数据格式



#### 响应参数 

##### 1)数据格式

```
{
  "code": 0,
  "message": "success",
  "data": {
    "id": 101,
    "did": "did:weid:1:0xccf92a3b5754ecbd441b5b1fbcc3a9db12d4fbcb",
    "created": 1634713316,
    "currentBlockNum": 164,
    "previousBlockNum": 163,
    "accValue": null,
    "name": null,
    "isAuthority": "NO"
  },
  "attachment": null
}
```

### 4.查询Cpt详情

#### 接口描述

查询Did的详细信息

#### 接口Url

http://localhost:6021/query/cpt/detail?cptId=11

#### 调用方法

HTTP GET

#### 请求参数

##### 1)参数表

| 参数      | 字段  | 举例 |
| --------- | ----- | ---- |
| CPT对应ID | cptId | 11   |

##### 2)数据格式



#### 响应参数

##### 1)数据格式

```
{
  "code": 0,
  "message": "success",
  "data": {
    "id": 1,
    "cptId": 11,
    "cptVersion": 1,
    "cptPublisher": "did:weid:1:0x8e5831ceb8451931a34b6d49a4bae8979d0b4041",
    "cptSignature": "AfWlduX6mcQZwhyyBSJgYf2wrDrGN11sCFaQSePDeqAYQE+nmH+rLI5EgNUQAVYxpCZBw/pzmw5gqf3EpdgPS6I=",
    "updated": 0,
    "created": 1633941876
  },
  "attachment": null
}
```

### 5.还提供了查询所有Did、Cpt、Document、Total的接口，可根据需要使用。





## 数据库表

## TotalInfo

|              |                  |          |
| ------------ | ---------------- | -------- |
| **名称**     | **字段**         | **类型** |
| 主键         | id               | bigint   |
| DID总数      | did_count        | int      |
| CPT总数      | cpt_count        | int      |
| 签发者总数   | issuer_count     | int      |
| 认证签发总数 | recognized_count | int      |

## DidInfo

|                |                    |          |
| -------------- | ------------------ | -------- |
| **名称**       | **字段**           | **类型** |
| 主键           | id                 | bigint   |
| DID            | did                | varchar  |
| DID所有名称    | name               | varchar  |
| 创建时间       | created            | bigint   |
| 是否权威       | is_authority       | varchar  |
| 当前区块       | current_block_num  | int      |
| 前一区块       | previous_block_num | int      |
| 权威签发计算值 | acc_value          | varchar  |

## CptInfo

|               |               |          |
| ------------- | ------------- | -------- |
| **名称**      | **字段**      | **类型** |
| 主键          | id            | bigint   |
| CPT对应ID     | cpt_id        | int      |
| CPT对应签发者 | cpt_publisher | varchar  |
| CPT对应签名   | cpt_signature | varchar  |
| CPT版本       | cpt_version   | int      |
| CPT创建时间   | created       | bigint   |
| CPT更新时间   | updated       | bigint   |

## DocumentInfo

| **名称**      | **字段**      | **类型** |
| ------------- | ------------- | -------- |
| 主键          | id            | bigint   |
| CPT对应ID     | cpt_id        | int      |
| CPT对应签发者 | cpt_publisher | varchar  |
| CPT对应签名   | cpt_signature | varchar  |
| CPT版本       | cpt_version   | int      |
| CPT创建时间   | created       | bigint   |
| CPT更新时间   | updated       | bigint   |

## ServiceInfo

| **名称** | **字段**         | **类型** |
| -------- | ---------------- | -------- |
| 主键     | id               | bigint   |
| 服务终端 | service_endpoint | varchar  |
| 类型     | type             | varchar  |

