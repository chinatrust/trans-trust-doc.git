# 组件介绍

## 总体介绍

TransTrust SDK提供了一套对TransTrust进行管理操作的Java库。目前，SDK支持本地密钥管理、数字身份标识（WeIdentity DID）管理、  授权机构（Authority Issuer）管理、 凭证模板（CPT）管理、凭证（Credential）管理（包括可信签发验证、选择性披露、链下加密存储和流转管理）、链下数据库管理等功能， 同时也提供基于FISCO-BCOS 2.0的区块链交互、智能合约的部署与调用。 未来还将支持更丰富的功能和应用。

主要模块：

| 模块 | 注释 |
| -- | ----- |
| DID服务 | 提供DID注册等功能 |
| CPT服务 | 提供CPT注册、查询和更新等功能 |
| Credential服务 | 提供Credential签发、验证、计算哈希和选择性披露等功能 |
