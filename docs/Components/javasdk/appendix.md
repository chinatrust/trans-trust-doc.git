# 附录

## CTCptService相关的参数类型描述

#### CTCptMapArgs

注册CPT所需的构建信息，包含CPT的必要信息。

```java
CTCptMapArgs {
    private WeIdAuthentication weIdAuthentication;

    private CptType cptType = CptType.ORIGINAL;

    private String cptName;

    private String cptDescription;

    private String cptManagment;

    private Map<String, Object> credentialType;

    private String url;

    private String supervisable;

    private Map<String, Object> properties;;

    private String[] required = null;
}
```

#### Cpt

CPT的完整信息，包含构建CPT传入的内容和系统补全的内容比如CPT基本信息和一些元数据。

```java
Cpt {
    /**
     * Base info of cpt.
     */
    protected CptBaseInfo cptBaseInfo = new CptBaseInfo();

    /**
     * The cpt json schema.
     */
    protected Map<String, Object> cptJsonSchema;

    /**
     * The meta data.
     */
    protected MetaData metaData = new MetaData();
}
```

其中cptJsonSchema主要是注册CPT时所传入的CTCptMapArgs的内容，元数据MetaData包含创建时间、过期时间、创建者信息和创建者的签名。

```java
MetaData {
    /**
     * The weIdentity DID of the publisher who register this CPT.
     */
    private String cptPublisher;

    /**
     * The cpt signature for the weIdentity DID and json schema data in Base64.
     */
    private String cptSignature;

    /**
     * The cpt create timestamp.
     */
    private long created;

    /**
     * The cpt update timestamp.
     */
    private long updated;
}
```

## CTCredentialService相关的参数类型描述
#### CTCreateCredentialPojoArgs

签发Credential所需的构建信息，包含所依据的CPT ID、Credential的必要内容和该CPT自定义业务属性的内容（claim部分）。

```java
CTCreateCredentialPojoArgs {

    private static final Logger logger = LoggerFactory.getLogger(CTCreateCredentialPojoArgs.class);

    private WeIdAuthentication weIdAuthentication;

    private Integer cptId;

    private Long expirationDate;

    private String id = null;

    private Map<String, Object> claim;

    private Map<String, String> recipient = null;

    private CredentialType docType = CredentialType.ORIGINAL;
}
```

#### CTCredentialPojo

生成的Credential的完整信息，包含所依据的CPT的全部内容（CptBaseInfo、CTCptMapArgs和MetaData）、Credential的构建参数内容properties和系统自动补全的信息（Credential的签发时间和proof等等）。

```java
CTCredentialPojo {

    private String $schema = JsonSchemaConstant.SCHEMA_VALUE;

    private String type = JsonSchemaConstant.DATA_TYPE_OBJECT;

    private CptBaseInfo cptBaseInfo = new CptBaseInfo();

    private CTCptMapArgs ctCptMapArgs = new CTCptMapArgs();

    private Cpt.MetaData metaData = new Cpt.MetaData();

    private Map<String, Object> properties;

    private String context;

    private List<String> docType;

    private Map<String, Object> proof;
}
```

其中proof中包含了签发者信息、签发时间、对应claim部分的哈希盐和签名，示例如下。
```json
"proof":{
        "created":"2021-10-20T14:46:32Z",
        "creator":"did:weid:1:0x6b472e796f9819a65621ad3c1ce47f680294a917key-0",
        "salt":{
            "certificateNumber":"1ia5o",
            "exclusiveArea":"y2syZ",
            "gettingPrice":"2o4cC",
            "holder":"yOaBp",
            "landNumber":"0Wvvo",
            "landType":"vUjcB",
            "sharingArea":"7xnJj",
            "site":"NHidD",
            "totalArea":"Y7p9g",
            "useType":"hfb3A"
        },
        "signatureValue":"dv0TgTJti9L28qUjBcLKiNgz2Gh7EkOchD2n83A3rVIaXqNQaZVHWwy+D80Z52h9pbgfVwRAqHmSnBmjn0bNzAA=",
        "type":"Secp256k1"
    }
```

#### ClaimPolicy

ClaimPolicy是选择性披露的策略，针对Credential中的业务属性字段claim的内容，指定哪些属性需要披露和哪些属性需要隐藏（1表示披露，0表示隐藏），ClaimPolicy本质上是一个字符串。

```java
ClaimPolicy {
    private String fieldsToBeDisclosed;
}
```

#### CTCredentialFomal

CTCredentialFomal是面向用户的具有规范格式的Credential，包含ChinaTrust Credential格式的必要信息。createCredential接口生成的CTCredentialPojo类型Credential可以直接通过内部方法转化成CTCredentialFomal，也可以由CTCredentialFomal转化成CTCredentialPojo。最后交给用户的也是CTCredentialFomal类型数据。

```java
CTCredentialFomal {

    private String $schema = JsonSchemaConstant.SCHEMA_VALUE;

    private String type = JsonSchemaConstant.DATA_TYPE_OBJECT;

    private CptType cptType = CptType.ORIGINAL;

    private String context;

    private Integer cptId;

    private Integer cptVersion;

    private String cptName;

    private String cptDescription;

    private String cptManagment;

    private Map<String, Object> credentialType;

    private String url;

    private String supervisable;

    private String cptPublisher;

    private String cptSignature;

    private long created;

    private long updated;

    private Map<String, Object> properties;

    private Map<String, Object> proof;
}
```
