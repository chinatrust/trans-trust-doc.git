# 接口文档

## 基本数据结构

### 返回值

- com.webank.weid.protocol.response.ResponseData<T>

  | 名称            | 类型            | 说明         | 备注 |
  | --------------- | --------------- | ------------ | ---- |
  | errorCode       | Integer         | 返回结果码   |      |
  | errorMessage    | String          | 返回结果描述 |      |
  | result          | T               | 具体对象     |      |
  | transactionInfo | TransactionInfo | 交易信息     | 见下 |

- com.webank.weid.protocol.response.TransactionInfo

  | 名称             | 类型       | 说明     | 备注 |
  | ---------------- | ---------- | -------- | ---- |
  | blockNumber      | BigInteger | 交易块高 |      |
  | transactionHash  | String     | 交易hash |      |
  | transactionIndex | BigInteger | 交易索引 |      |

## 1 工具接口

### 1.1 查询链类型

#### **基本信息**

```
接口名称: com.chinatrust.sdk.client.CTToolClient.getEncryptType
接口定义: ResponseData<Integer> getEncryptType();
接口描述: 查询链类型，返回（0-非国密，1-国密）
```

#### 传入参数

#### 返回参数

| 名称   | 类型    | 说明             | 备注 |
| ------ | ------- | ---------------- | ---- |
| result | Integer | 0-非国密，1-国密 |      |

#### 调用示例

```java
  CTToolClient ctToolClient = new CTToolService();
  ResponseData<Integer> encryptType = ctToolClient.getEncryptType();
```

- 返回示例

```
{
  "result": 0,
  "errorCode": 0,
  "errorMessage": "success",
  "transactionInfo": null
}
```

### 1.2 创建公私钥对

#### **基本信息**

```
接口名称: com.chinatrust.sdk.client.CTToolClient.createKeyPair
接口定义: ResponseData<CTECKeyResult> createKeyPair;
接口描述: 创建公私钥对，返回公私钥信息
```

#### 传入参数

#### 返回参数

| 名称   | 类型          | 说明 | 备注 |
| ------ | ------------- | ---- | ---- |
| result | CTECKeyResult |      | 见下 |

- com.chinatrust.sdk.client.CTECKeyResult

| 名称       | 类型   | 说明 | 备注 |
| ---------- | ------ | ---- | ---- |
| publicKey  | String | 公钥 |      |
| privateKey | String | 私钥 |      |

#### 调用示例

```java
  CTToolClient ctToolClient = new CTToolService();
  ResponseData<CTECKeyResult> keyPair = ctToolClient.createKeyPair();
```

- 返回示例

```
{
  "result": {
    "publicKey": "3790025264149056804420750603831980189350992013212881402993548377721976435949467095867731903968304123687419511705432390500511029186762374812957156385919207",
    "privateKey": "59591253042657340926942978150921452694699180773497415854805668070729089636242"
  },
  "errorCode": 0,
  "errorMessage": "success",
  "transactionInfo": null
}
```

### 1.3 通过私钥查询公钥

#### **基本信息**

```
接口名称: com.chinatrust.sdk.client.CTToolClient.getPublicKey
接口定义: ResponseData<String> getPublicKey(String privateKey);
接口描述: 通过私钥查询公钥，返回公钥信息
```

#### 传入参数

| 名称       | 类型   | **非空** | 说明 | 备注 |
| ---------- | ------ | -------- | ---- | ---- |
| privateKey | String | Y        | 私钥 |      |

#### 返回参数

| 名称   | 类型   | 说明 | 备注 |
| ------ | ------ | ---- | ---- |
| result | String | 公钥 |      |

#### 调用示例

```java
  CTToolClient ctToolClient = new CTToolService();
  ResponseData<String> publicKey = ctToolClient.getPublicKey("7537941240201473300322488397004461834560927189780643296560422282794341761545");
```

- 返回示例

```
{
  "result": "4432445553701865582083821298682786108232857406963365256782233667063089026603279356140927749891030623113144876717056777356676658456163011484717338048459954",
  "errorCode": 0,
  "errorMessage": "success",
  "transactionInfo": null
}
```

## 2 初始化接口

### 2.1 部署WeID合约

#### **基本信息**

```
接口名称: com.chinatrust.sdk.client.CTInitClient.initCT
接口定义: ResponseData<ContractConfig> initCT(String chainId, String adminKey);
接口描述: 初始化链，部署WeID合约，返回相关合约地址
```

#### 传入参数

| 名称     | 类型   | **非空** | 说明                     | 备注 |
| -------- | ------ | -------- | ------------------------ | ---- |
| chainId  | String | Y        | 链编号（多链时用于区分） |      |
| adminKey | String | Y        | 链管理员私钥             |      |

#### 返回参数

| 名称   | 类型           | 说明         | 备注 |
| ------ | -------------- | ------------ | ---- |
| result | ContractConfig | 合约地址信息 | 见下 |

- com.webank.weid.config.ContractConfig

| 名称                  | 类型   | 说明                     | 备注                                     |
| --------------------- | ------ | ------------------------ | ---------------------------------------- |
| weIdAddress           | String | weId合约地址             |                                          |
| cptAddress            | String | cpt合约地址              |                                          |
| issuerAddress         | String | Authority Issuer合约地址 | 权限管理合约                             |
| evidenceAddress       | String | 存证合约地址             |                                          |
| specificIssuerAddress | String | 链上声明合约地址         | 为每位Authority Issuer在链上声明所属类型 |

#### 调用示例

```java
  CTInitClient cTInitClient = new CTInitService();
  ResponseData<ContractConfig> result = cTInitClient.initCT(100, "28354348400257293578085204282790324607891232742724770334587266909072589459877");
```

- 返回示例

```
{
  "result": {
  	"weIdAddress": "",
  	"cptAddress": "",
  	"issuerAddress": "",
  	"evidenceAddress": "",
  	"specificIssuerAddress": "",
  },
  "errorCode": 0,
  "errorMessage": "success",
  "transactionInfo": null
}
```

## 3. DID相关接口

### 3.1 创建DID(不包含参数)，自动生成公私钥

#### **基本信息**

```
接口名称: com.chinatrust.sdk.client.CTDIDClient.createWeId
接口定义: ResponseData<CreateWeIdDataResult> createWeId();
接口描述: 创建DID(不包含参数)，自动生成公私钥，返回DID信息，包含公私钥
```

#### 传入参数

#### 返回参数

| 名称   | 类型                 | 说明 | 备注 |
| ------ | -------------------- | ---- | ---- |
| result | CreateWeIdDataResult |      | 见下 |

- com.webank.weid.protocol.response.CreateWeIdDataResult

| 名称               | 类型           | 说明                         | 备注                           |
| ------------------ | -------------- | ---------------------------- | ------------------------------ |
| weId               | String         | 公钥WeIdentity DID格式字符串 | 格式: did:weid:1000:0x…………………. |
| userWeIdPublicKey  | WeIdPublicKey  |                              |                                |
| userWeIdPrivateKey | WeIdPrivateKey |                              |                                |

- com.webank.weid.protocol.base.WeIdPublicKey

| 名称      | 类型   | 说明     | 备注               |
| --------- | ------ | -------- | ------------------ |
| publicKey | String | 数字公钥 | 使用十进制数字表示 |

- com.webank.weid.protocol.base.WeIdPrivateKey

| 名称       | 类型   | 说明 | 备注               |
| ---------- | ------ | ---- | ------------------ |
| privateKey | String | 私钥 | 使用十进制数字表示 |

#### 调用示例

```java
  CTDIDClient ctDIDClient = new CTDIDService();
  ResponseData<CreateWeIdDataResult> responseData = ctDIDClient.createWeId();
```

- 返回示例

```
{
  "result": {
    "weId": "did:weid:100:0xe915b623e9a3b3693a6103130c16ade223ead7a4",
    "userWeIdPublicKey": {
      "publicKey": "10629306746229301646533384957716579696610523073854892232317686689945577054904335398283990329316678873708266098341877871910131709073940345972710147526890318"
    },
    "userWeIdPrivateKey": {
      "privateKey": "13104316464687572405485389309564698887244402079702979907539367492001438836277"
    }
  },
  "errorCode": 0,
  "errorMessage": "success",
  "transactionInfo": {
    "blockNumber": 791,
    "transactionHash": "0xde6372df86e3ae6b736beb860d284920101031658f955f1c4ffbd224dab3b4f5",
    "transactionIndex": 0
  }
}
```

### 3.2 根据公私钥创建DID

#### **基本信息**

```
接口名称: com.chinatrust.sdk.client.CTDIDClient.createWeId
接口定义: ResponseData<String> createWeId(CreateWeIdArgs createWeIdArgs);
接口描述: 根据公私钥创建DID，返回DID编号
```

#### 传入参数

- com.webank.weid.protocol.request.CreateWeIdArgs

| 名称           | 类型           | **非空** | 说明     | 备注 |
| -------------- | -------------- | -------- | -------- | ---- |
| publicKey      | String         | Y        | 数字公钥 |      |
| weIdPrivateKey | WeIdPrivateKey | Y        |          |      |

- com.webank.weid.protocol.base.WeIdPrivateKey

| 名称       | 类型   | 说明 | 备注               |
| ---------- | ------ | ---- | ------------------ |
| privateKey | String | 私钥 | 使用十进制数字表示 |

#### 返回参数

| 名称   | 类型   | 说明                         | 备注                         |
| ------ | ------ | ---------------------------- | ---------------------------- |
| result | String | 公钥WeIdentity DID格式字符串 | 如：did:weid:1000:0x…………………. |

#### 调用示例

```java
  CTDIDClient ctDIDClient = new CTDIDService();
  CreateWeIdArgs createWeIdArgs = new CreateWeIdArgs();
  createWeIdArgs.setPublicKey("8205420317873094558027547996078286084567502231666295510052917878556286408790255249743121224706216932755118145659114522743249996489418640899296590250285724");
  createWeIdArgs.setWeIdPrivateKey(new WeIdPrivateKey("28354348400257293578085204282790324607891232742724770334587266909072589459877"));
  ResponseData<String> createResult = ctDIDClient.createWeId(createWeIdArgs);
```

- 返回示例

```
{
  "result": "did:weid:100:0x1cd7fa8518c82d5b87fd5a150930a30471b58906",
  "errorCode": 0,
  "errorMessage": "success",
  "transactionInfo": {
    "blockNumber": 792,
    "transactionHash": "0xd34c09f4539b278a7f5bfa189eb9e523cce8d55cced24aa4f4445b9107b9224f",
    "transactionIndex": 0
  }
}
```

### 3.3 查询DID Document对象

#### **基本信息**

```
接口名称: com.chinatrust.sdk.client.CTDIDClient.getWeIdDocument
接口定义: ResponseData<WeIdDocument> getWeIdDocument(String weId);
接口描述: 根据WeIdentity DID查询出WeIdentity DID Document对象
```

#### 传入参数

| 名称 | 类型   | **非空** | 说明                 | 备注 |
| ---- | ------ | -------- | -------------------- | ---- |
| weId | String | Y        | WeIdentity DID字符串 |      |

#### 返回参数

| 名称   | 类型         | 说明 | 备注 |
| ------ | ------------ | ---- | ---- |
| result | WeIdDocument |      | 见下 |

- com.webank.weid.protocol.base.WeIdDocument

| 名称           | 类型   | 说明           | 备注               |
| -------------- | ------ | -------------- | ------------------ |
| id             | String | WeIdentity DID |                    |
| created        | Long   | 创建时间       |                    |
| updated        | Long   | 更新时间       |                    |
| publicKey      | List   |                | 列出公钥集合，见下 |
| authentication | List   |                | 认证方集合，见下   |
| service        | List   |                | 服务端点集合，见下 |

- com.webank.weid.protocol.base.PublicKeyProperty

| 名称      | 类型   | 说明                 | 备注              |
| --------- | ------ | -------------------- | ----------------- |
| id        | String |                      |                   |
| type      | String | 类型                 | 默认为：Secp256k1 |
| owner     | String | 拥有者WeIdentity DID |                   |
| publicKey | String | 数字公钥             |                   |

- com.webank.weid.protocol.base.AuthenticationProperty

| 名称      | 类型   | 说明 | 备注              |
| --------- | ------ | ---- | ----------------- |
| type      | String | 类型 | 默认为：Secp256k1 |
| publicKey | String |      |                   |

- com.webank.weid.protocol.base.ServiceProperty

| 名称            | 类型   | 说明 | 备注 |
| --------------- | ------ | ---- | ---- |
| type            | String | 类型 |      |
| serviceEndpoint | String |      |      |

#### 调用示例

```java
  CTDIDClient ctDIDClient = new CTDIDService();
  ResponseData<CreateWeIdDataResult> responseData = ctDIDClient.createWeId();
  ResponseData<WeIdDocument> weIdDocument = ctDIDClient.getWeIdDocument(responseData.getResult().getWeId());
```

- 返回示例

```
{
  "result": {
      "authentication": [
        {
          "publicKey": "did:weid:1:0x08050ca42b3c19e0a1378b5cb731ccdffee83076#keys-0",
          "revoked": false,
          "type": "Secp256k1"
        }
      ],
      "created": 1637918903,
      "id": "did:weid:1:0x08050ca42b3c19e0a1378b5cb731ccdffee83076",
      "publicKey": [
        {
          "id": "did:weid:1:0x08050ca42b3c19e0a1378b5cb731ccdffee83076#keys-0",
          "owner": "did:weid:1:0x08050ca42b3c19e0a1378b5cb731ccdffee83076",
          "publicKey": "10628163836159724890492840664578817091818462350513430135306180953019304386546878505553946154614735801032955641977230040981649371058690313466953228250212062",
          "revoked": false,
          "type": "Secp256k1"
        }
      ],
      "service": [

      ],
      "updated": null,
      "$from": "toJson"
    },
  "errorCode": 0,
  "errorMessage": "success",
  "transactionInfo": null
}
```

### 3.4 查询DID是否存在

#### **基本信息**

```
接口名称: com.chinatrust.sdk.client.CTDIDClient.isWeIdExist
接口定义: ResponseData<Boolean> isWeIdExist(String weId);
接口描述: 根据WeIdentity DID判断链上是否存在
```

#### 传入参数

| 名称 | 类型   | **非空** | 说明                 | 备注                  |
| ---- | ------ | -------- | -------------------- | --------------------- |
| weId | String | Y        | WeIdentity DID字符串 | 如：did:weid:101:0x…. |

#### 返回参数

| 名称   | 类型    | 说明     | 备注 |
| ------ | ------- | -------- | ---- |
| result | Boolean | 是否存在 |      |

#### 调用示例

```java
  CTDIDClient ctDIDClient = new CTDIDService();
  ResponseData<CreateWeIdDataResult> responseData = ctDIDClient.createWeId();
  ResponseData<Boolean> isWeIdExist = ctDIDClient.isWeIdExist(responseData.getResult().getWeId());
```

- 返回示例

```
{
  "result": true,
  "errorCode": 0,
  "errorMessage": "success",
  "transactionInfo": null
}
```

## 4. Cpt相关接口

### 4.1 注册CPT

#### **基本信息**

```
接口名称: com.chinatrust.sdk.client.CTCptClient.registerCpt
接口定义: ResponseData<CptBaseInfo> registerCpt(CTCptMapArgs args);
接口描述: 通过传入的CPT构建参数，在链上的CPT管理合约注册CPT
```

#### 传入参数

- com.chinatrust.sdk.client.CTCptMapArgs

| 名称               | 类型                | 非空 | 说明                                      | 备注 |
| ------------------ | ------------------- | ---- | ----------------------------------------- | ---- |
| weIdAuthentication | WeIdAuthentication  | Y    | 认证信息，包含WeIdentity DID和私钥        | 见下 |
| cptName            | String              | Y    | CPT名字                                   |      |
| cptDescription     | String              | Y    | CPT描述                                   |      |
| cptManagment       | String              | Y    | 权限管理合约地址                          |      |
| credentialType     | Map<String, Object> | Y    | 文档类型（可信文档/可流转文档）           |      |
| url                | String              | Y    | 查看或申请基于该CPT签发的credential的网址 |      |
| supervisable       | String              | Y    | 是否支持监管（true/false）                |      |
| properties         | Map<String, Object> | Y    | 必要属性                                  |      |
| required           | String[]            | Y    | 具体业务属性字段                          |      |

- com.webank.weid.protocol.base.WeIdAuthentication

| 名称            | 类型           | 非空 | 说明                      | 备注                     |
| --------------- | -------------- | ---- | ------------------------- | ------------------------ |
| weId            | String         | Y    | CPT发布者的WeIdentity DID | WeIdentity DID的格式传入 |
| weIdPublicKeyId | String         | N    | 公钥Id                    |                          |
| weIdPrivateKey  | WeIdPrivateKey | Y    |                           | 交易私钥，见下           |

- com.webank.weid.protocol.base.WeIdPrivateKey

| 名称       | 类型   | 说明 | 备注               |
| ---------- | ------ | ---- | ------------------ |
| privateKey | String | 私钥 | 使用十进制数字表示 |

#### 返回参数

| 名称   | 类型        | 说明 | 备注              |
| ------ | ----------- | ---- | ----------------- |
| result | CptBaseInfo |      | CPT基础数据，见下 |

- com.webank.weid.protocol.base.CptBaseInfo

| 名称       | 类型    | 说明     | 备注 |
| ---------- | ------- | -------- | ---- |
| cptId      | Integer | cpId编号 |      |
| cptVersion | Integer | 版本号   |      |

#### 调用示例

```java
  CTCptMapArgs ctCptMapArgs = new CTCptMapArgs();
  
  WeIdAuthentication weIdAuthentication = new WeIdAuthentication();
  weIdAuthentication.setWeId("did:weid:100:0xc204e875442db20b7316f726f0c1dccae7d39b3d");
  weIdAuthentication.setWeIdPrivateKey(new WeIdPrivateKey("7537941240201473300322488397004461834560927189780643296560422282794341761545"));
  ctCptMapArgs.setWeIdAuthentication(weIdAuthentication);
  
  ctCptMapArgs.setCptName("landcertificate");
  ctCptMapArgs.setCptDescription("unit land certificate");
  ctCptMapArgs.setCptManagment("0xabcdef1111");
  ctCptMapArgs.setUrl("http://TransTrust.com");
  ctCptMapArgs.setSupervisable("false");

  Map<String, Object> credentialType = new HashMap<String, Object>(1);
  credentialType.put("cptTradeToken", "0xabcdef2222");
  ctCptMapArgs.setCredentialType(credentialType);
  
  Map<String, Object> propertites = new HashMap<String, Object>(6);
  
  HashMap<String, Object> claim = new HashMap<String, Object>(3);
  HashMap<String, Object> claimMap1 = new HashMap<String, Object>(2);
  claimMap1.put(JsonSchemaConstant.TYPE_KEY, JsonSchemaConstant.DATA_TYPE_STRING);
  claimMap1.put("description", "certificate number");
  HashMap<String, Object> claimMap2 = new HashMap<String, Object>(2);
  claimMap2.put(JsonSchemaConstant.TYPE_KEY, JsonSchemaConstant.DATA_TYPE_STRING);
  claimMap2.put("description", "land type");
  HashMap<String, Object> claimMap3 = new HashMap<String, Object>(2);
  claimMap3.put(JsonSchemaConstant.TYPE_KEY, JsonSchemaConstant.DATA_TYPE_STRING);
  claimMap3.put("description", "total area");
  HashMap<String, Object> claimMap4 = new HashMap<String, Object>(2);
  claimMap4.put(JsonSchemaConstant.TYPE_KEY, JsonSchemaConstant.DATA_TYPE_STRING);
  claimMap4.put("description", "exclusive area");
  HashMap<String, Object> claimMap5 = new HashMap<String, Object>(2);
  claimMap5.put(JsonSchemaConstant.TYPE_KEY, JsonSchemaConstant.DATA_TYPE_STRING);
  claimMap5.put("description", "sharing area");
  HashMap<String, Object> claimMap6 = new HashMap<String, Object>(2);
  claimMap6.put(JsonSchemaConstant.TYPE_KEY, JsonSchemaConstant.DATA_TYPE_STRING);
  claimMap6.put("description", "use type");
  HashMap<String, Object> claimMap7 = new HashMap<String, Object>(2);
  claimMap7.put(JsonSchemaConstant.TYPE_KEY, JsonSchemaConstant.DATA_TYPE_STRING);
  claimMap7.put("description", "land number");
  HashMap<String, Object> claimMap8 = new HashMap<String, Object>(2);
  claimMap8.put(JsonSchemaConstant.TYPE_KEY, JsonSchemaConstant.DATA_TYPE_STRING);
  claimMap8.put("description", "holder");
  HashMap<String, Object> claimMap9 = new HashMap<String, Object>(2);
  claimMap9.put(JsonSchemaConstant.TYPE_KEY, JsonSchemaConstant.DATA_TYPE_STRING);
  claimMap9.put("description", "getting price");
  HashMap<String, Object> claimMap10 = new HashMap<String, Object>(2);
  claimMap10.put(JsonSchemaConstant.TYPE_KEY, JsonSchemaConstant.DATA_TYPE_STRING);
  claimMap10.put("description", "site");
  claim.put("certificateNumber", claimMap1);
  claim.put("landType", claimMap2);
  claim.put("totalArea", claimMap3);
  claim.put("exclusiveArea", claimMap4);
  claim.put("sharingArea", claimMap5);
  claim.put("useType", claimMap6);
  claim.put("landNumber", claimMap7);
  claim.put("holder", claimMap8);
  claim.put("gettingPrice", claimMap9);
  claim.put("site", claimMap10);
  
  propertites.put("claim", claim);
  ctCptMapArgs.setProperties(propertites);
  
  String[] required = { "landType", "totalArea" };
  ctCptMapArgs.setRequired(required);

  CTCptService ctCptService = new CTCptService();
  ResponseData<CptBaseInfo> response = ctCptService.registerCpt(ctCptMapArgs);
```

- 返回示例

```
{
  "result": {
    "cptId": 2000000,
    "cptVersion": 1
  },
  "errorCode": 0,
  "errorMessage": "success",
  "transactionInfo": {
    "blockNumber": 778,
    "transactionHash": "0x103ede138aaaa25d86556bf4b82ffe701a69c2bade1d6b103bccae555a5f1f1d",
    "transactionIndex": 0
  }
}
```

### 4.2 注册指定ID的CPT

#### **基本信息**

```
接口名称: com.chinatrust.sdk.client.CTCptClient.registerCpt
接口定义: ResponseData<CptBaseInfo> registerCpt(CTCptMapArgs args, Integer cptId);
接口描述: 通过传入的CPT构建参数和指定的CPT ID在链上的CPT管理合约注册CPT
```

#### 传入参数

| 名称  | 类型         | 非空 | 说明        | 备注      |
| ----- | ------------ | ---- | ----------- | --------- |
| args  | CTCptMapArgs | Y    | CPT构建参数 | 见4.1入参 |
| cptId | Integer      | Y    | CPT ID      |           |

#### 返回参数

| 名称   | 类型        | 说明 | 备注              |
| ------ | ----------- | ---- | ----------------- |
| result | CptBaseInfo |      | CPT基础数据，见下 |

- com.webank.weid.protocol.base.CptBaseInfo

| 名称       | 类型    | 说明     | 备注 |
| ---------- | ------- | -------- | ---- |
| cptId      | Integer | cpId编号 |      |
| cptVersion | Integer | 版本号   |      |

#### 调用示例

```java
  CTCptMapArgs ctCptMapArgs = new CTCptMapArgs();
  
  WeIdAuthentication weIdAuthentication = new WeIdAuthentication();
  weIdAuthentication.setWeId("did:weid:100:0xc204e875442db20b7316f726f0c1dccae7d39b3d");
  weIdAuthentication.setWeIdPrivateKey(new WeIdPrivateKey("7537941240201473300322488397004461834560927189780643296560422282794341761545"));
  ctCptMapArgs.setWeIdAuthentication(weIdAuthentication);
  
  ctCptMapArgs.setCptName("landcertificate");
  ctCptMapArgs.setCptDescription("unit land certificate");
  ctCptMapArgs.setCptManagment("0xabcdef1111");
  ctCptMapArgs.setUrl("http://TransTrust.com");
  ctCptMapArgs.setSupervisable("false");
  Map<String, Object> credentialType = new HashMap<String, Object>(1);
  credentialType.put("cptTradeToken", "0xabcdef2222");
  ctCptMapArgs.setCredentialType(credentialType);
  
  Map<String, Object> propertites = new HashMap<String, Object>(6);
  
  HashMap<String, Object> claim = new HashMap<String, Object>(3);
  HashMap<String, Object> claimMap1 = new HashMap<String, Object>(2);
  claimMap1.put(JsonSchemaConstant.TYPE_KEY, JsonSchemaConstant.DATA_TYPE_STRING);
  claimMap1.put("description", "certificate number");
  HashMap<String, Object> claimMap2 = new HashMap<String, Object>(2);
  claimMap2.put(JsonSchemaConstant.TYPE_KEY, JsonSchemaConstant.DATA_TYPE_STRING);
  claimMap2.put("description", "land type");
  HashMap<String, Object> claimMap3 = new HashMap<String, Object>(2);
  claimMap3.put(JsonSchemaConstant.TYPE_KEY, JsonSchemaConstant.DATA_TYPE_STRING);
  claimMap3.put("description", "total area");
  HashMap<String, Object> claimMap4 = new HashMap<String, Object>(2);
  claimMap4.put(JsonSchemaConstant.TYPE_KEY, JsonSchemaConstant.DATA_TYPE_STRING);
  claimMap4.put("description", "exclusive area");
  HashMap<String, Object> claimMap5 = new HashMap<String, Object>(2);
  claimMap5.put(JsonSchemaConstant.TYPE_KEY, JsonSchemaConstant.DATA_TYPE_STRING);
  claimMap5.put("description", "sharing area");
  HashMap<String, Object> claimMap6 = new HashMap<String, Object>(2);
  claimMap6.put(JsonSchemaConstant.TYPE_KEY, JsonSchemaConstant.DATA_TYPE_STRING);
  claimMap6.put("description", "use type");
  HashMap<String, Object> claimMap7 = new HashMap<String, Object>(2);
  claimMap7.put(JsonSchemaConstant.TYPE_KEY, JsonSchemaConstant.DATA_TYPE_STRING);
  claimMap7.put("description", "land number");
  HashMap<String, Object> claimMap8 = new HashMap<String, Object>(2);
  claimMap8.put(JsonSchemaConstant.TYPE_KEY, JsonSchemaConstant.DATA_TYPE_STRING);
  claimMap8.put("description", "holder");
  HashMap<String, Object> claimMap9 = new HashMap<String, Object>(2);
  claimMap9.put(JsonSchemaConstant.TYPE_KEY, JsonSchemaConstant.DATA_TYPE_STRING);
  claimMap9.put("description", "getting price");
  HashMap<String, Object> claimMap10 = new HashMap<String, Object>(2);
  claimMap10.put(JsonSchemaConstant.TYPE_KEY, JsonSchemaConstant.DATA_TYPE_STRING);
  claimMap10.put("description", "site");
  claim.put("certificateNumber", claimMap1);
  claim.put("landType", claimMap2);
  claim.put("totalArea", claimMap3);
  claim.put("exclusiveArea", claimMap4);
  claim.put("sharingArea", claimMap5);
  claim.put("useType", claimMap6);
  claim.put("landNumber", claimMap7);
  claim.put("holder", claimMap8);
  claim.put("gettingPrice", claimMap9);
  claim.put("site", claimMap10);
  
  propertites.put("claim", claim);
  ctCptMapArgs.setProperties(propertites);
  
  String[] required = { "landType", "totalArea" };
  ctCptMapArgs.setRequired(required);

  CTCptService ctCptService = new CTCptService();
  ResponseData<CptBaseInfo> response = ctCptService.registerCpt(ctCptMapArgs, 2000000);
```

- 返回示例

```
{
  "result": {
    "cptId": 2000000,
    "cptVersion": 1
  },
  "errorCode": 0,
  "errorMessage": "success",
  "transactionInfo": {
    "blockNumber": 778,
    "transactionHash": "0x103ede138aaaa25d86556bf4b82ffe701a69c2bade1d6b103bccae555a5f1f1d",
    "transactionIndex": 0
  }
}
```

### 4.3 查询CPT

#### **基本信息**

```
接口名称: com.chinatrust.sdk.client.CTCptClient.queryCpt
接口定义: ResponseData<Cpt> queryCpt(Integer cptId);
接口描述: 根据CPT ID查询对应CPT的详细信息
```

#### 传入参数

| 名称  | 类型    | 非空 | 说明   | 备注 |
| ----- | ------- | ---- | ------ | ---- |
| cptId | Integer | Y    | CPT ID |      |

#### 返回参数

| 名称   | 类型 | 说明 | 备注          |
| ------ | ---- | ---- | ------------- |
| result | Cpt  |      | CPT内容，见下 |

- com.webank.weid.protocol.base.Cpt

| 名称          | 类型                | 说明                       | 备注                  |
| ------------- | ------------------- | -------------------------- | --------------------- |
| cptJsonSchema | Map<String, Object> | Map类型的cptJsonSchema信息 |                       |
| cptBaseInfo   | CptBaseInfo         |                            | CPT基础数据，见下     |
| cptMetaData   | CptMetaData         |                            | CPT元数据内部类，见下 |

- com.webank.weid.protocol.base.CptBaseInfo

| 名称       | 类型    | 说明     | 备注 |
| ---------- | ------- | -------- | ---- |
| cptId      | Integer | cpId编号 |      |
| cptVersion | Integer | 版本号   |      |

- com.webank.weid.protocol.base.Cpt.MetaData

| 名称         | 类型   | 说明                      | 备注                                      |
| ------------ | ------ | ------------------------- | ----------------------------------------- |
| cptPublisher | String | CPT发布者的WeIdentity DID | WeIdentity DID格式数据                    |
| cptSignature | String | 签名数据                  | cptPublisher与cptJsonSchema拼接的签名数据 |
| updated      | long   | 更新时间                  |                                           |
| created      | long   | 创建日期                  |                                           |

#### 调用示例

```java
  CTCptService ctCptService = new CTCptService();
  ResponseData<Cpt> cptResponseData = ctCptClient.queryCpt(2000000);
```

- 返回示例

```
{
  "result": {
    "cptBaseInfo": {
      "cptId": 2000000,
      "cptVersion": 2
    },
    "cptJsonSchema": {
      "cptDescription": "unit land certificate",
      "credentialType": {
        "cptTradeToken": "0x45d617c5157e9a07ac570d18692d362c3e50c852"
      },
      "cptType": "original",
      "$schema": "http://json-schema.org/draft-04/schema#",
      "cptManagment": "0x57bc072ab8af771c0978f94bd3c9c76ad0cc6eff",
      "supervisable": "false",
      "type": "object",
      "cptName": "landcertificate",
      "properties": {
        "claim": {
          "certificateNumber": {
            "description": "certificate number",
            "type": "string"
          },
          "exclusiveArea": {
            "description": "exclusive area",
            "type": "string"
          },
          "gettingPrice": {
            "description": "getting price",
            "type": "string"
          },
          "holder": {
            "description": "holder",
            "type": "string"
          },
          "landNumber": {
            "description": "land number",
            "type": "string"
          },
          "landType": {
            "description": "land type",
            "type": "string"
          },
          "sharingArea": {
            "description": "sharing area",
            "type": "string"
          },
          "site": {
            "description": "site",
            "type": "string"
          },
          "totalArea": {
            "description": "total area",
            "type": "string"
          },
          "useType": {
            "description": "use type",
            "type": "string"
          }
        },
        "expirationDate": {
          "description": "expiration date",
          "type": "string"
        },
        "id": {
          "description": "credential id",
          "type": "string"
        },
        "issuanceDate": {
          "description": "issuance date",
          "type": "string"
        },
        "issuer": {
          "WeID": {
            "description": "the WeID of credential recipient",
            "type": "string"
          },
          "description": "the information of credential recipient",
          "name": {
            "description": "the name of credential recipient",
            "type": "string"
          },
          "type": "object"
        },
        "recipient": {
          "WeID": {
            "description": "the WeID of credential recipient",
            "type": "string"
          },
          "description": "the information of credential recipient",
          "name": {
            "description": "the name of credential recipient",
            "type": "string"
          },
          "type": "object"
        }
      },
      "required": [
        "landType",
        "totalArea"
      ],
      "url": "http://chinatrust.com"
    },
    "metaData": {
      "cptPublisher": "did:weid:100:0xc204e875442db20b7316f726f0c1dccae7d39b3d",
      "cptSignature": "ABnVA5FTH2AqfAOzL41TgTf2bra4Enm7idz+BxsjhXMSbfCwvK4VWuUYIwcVKnyKdRdcMAj1pYIC1TLXy96Ax+E=",
      "created": 1637221901,
      "updated": 0
    },
    "cptId": 2000000,
    "cptSignature": "ABnVA5FTH2AqfAOzL41TgTf2bra4Enm7idz+BxsjhXMSbfCwvK4VWuUYIwcVKnyKdRdcMAj1pYIC1TLXy96Ax+E=",
    "created": 1637221901,
    "updated": 0,
    "cptPublisher": "did:weid:100:0xc204e875442db20b7316f726f0c1dccae7d39b3d",
    "cptVersion": 2
  },
  "errorCode": 0,
  "errorMessage": "success",
  "transactionInfo": null
}
```

### 4.4 更新CPT

#### 基本信息

```
接口名称: com.chinatrust.sdk.client.CTCptClient.updateCpt
接口定义: ResponseData<CptBaseInfo> updateCpt(CTCptMapArgs args, Integer cptId);
接口描述: 更新指定CPT ID的CPT的内容
```

#### 传入参数

| 名称  | 类型         | 非空 | 说明        | 备注 |
| ----- | ------------ | ---- | ----------- | ---- |
| args  | CTCptMapArgs | Y    | CPT构建参数 | 见下 |
| cptId | Integer      | Y    | CPT ID      |      |

- com.chinatrust.sdk.client.CTCptMapArgs

| 名称               | 类型                | 非空 | 说明                                      | 备注 |
| ------------------ | ------------------- | ---- | ----------------------------------------- | ---- |
| weIdAuthentication | WeIdAuthentication  | Y    | 认证信息，包含WeIdentity DID和私钥        | 见下 |
| cptName            | String              | Y    | CPT名字                                   |      |
| cptDescription     | String              | Y    | CPT描述                                   |      |
| cptManagment       | String              | Y    | 权限管理合约地址                          |      |
| credentialType     | Map<String, Object> | Y    | 文档类型（可信文档/可流转文档）           |      |
| url                | String              | Y    | 查看或申请基于该CPT签发的credential的网址 |      |
| supervisable       | String              | Y    | 是否支持监管（true/false）                |      |
| properties         | Map<String, Object> | Y    | 必要属性                                  |      |
| required           | String[]            | Y    | 具体业务属性字段                          |      |

- com.webank.weid.protocol.base.WeIdAuthentication

| 名称            | 类型           | 非空 | 说明                      | 备注                     |
| --------------- | -------------- | ---- | ------------------------- | ------------------------ |
| weId            | String         | Y    | CPT发布者的WeIdentity DID | WeIdentity DID的格式传入 |
| weIdPublicKeyId | String         | N    | 公钥Id                    |                          |
| weIdPrivateKey  | WeIdPrivateKey | Y    |                           | 交易私钥，见下           |

- com.webank.weid.protocol.base.WeIdPrivateKey

| 名称       | 类型   | 说明 | 备注               |
| ---------- | ------ | ---- | ------------------ |
| privateKey | String | 私钥 | 使用十进制数字表示 |

#### 返回参数

| 名称   | 类型        | 说明 | 备注              |
| ------ | ----------- | ---- | ----------------- |
| result | CptBaseInfo |      | CPT基础数据，见下 |

- com.webank.weid.protocol.base.CptBaseInfo

| 名称       | 类型    | 说明     | 备注 |
| ---------- | ------- | -------- | ---- |
| cptId      | Integer | cpId编号 |      |
| cptVersion | Integer | 版本号   |      |

#### 调用示例

```java
  CTCptMapArgs ctCptMapArgs = new CTCptMapArgs();
  
  WeIdAuthentication weIdAuthentication = new WeIdAuthentication();
  weIdAuthentication.setWeId("did:weid:100:0xc204e875442db20b7316f726f0c1dccae7d39b3d");
  weIdAuthentication.setWeIdPrivateKey(new WeIdPrivateKey("7537941240201473300322488397004461834560927189780643296560422282794341761545"));
  ctCptMapArgs.setWeIdAuthentication(weIdAuthentication);
  
  ctCptMapArgs.setCptName("landcertificate");
  ctCptMapArgs.setCptDescription("unit land certificate");
  ctCptMapArgs.setCptManagment("0xabcdef1111");
  ctCptMapArgs.setUrl("http://TransTrust.com");
  ctCptMapArgs.setSupervisable("false");
  Map<String, Object> credentialType = new HashMap<String, Object>(1);
  credentialType.put("cptTradeToken", "0xabcdef2222");
  ctCptMapArgs.setCredentialType(credentialType);
  
  Map<String, Object> propertites = new HashMap<String, Object>(6);
  
  HashMap<String, Object> claim = new HashMap<String, Object>(3);
  HashMap<String, Object> claimMap1 = new HashMap<String, Object>(2);
  claimMap1.put(JsonSchemaConstant.TYPE_KEY, JsonSchemaConstant.DATA_TYPE_STRING);
  claimMap1.put("description", "certificate number");
  HashMap<String, Object> claimMap2 = new HashMap<String, Object>(2);
  claimMap2.put(JsonSchemaConstant.TYPE_KEY, JsonSchemaConstant.DATA_TYPE_STRING);
  claimMap2.put("description", "land type");
  HashMap<String, Object> claimMap3 = new HashMap<String, Object>(2);
  claimMap3.put(JsonSchemaConstant.TYPE_KEY, JsonSchemaConstant.DATA_TYPE_STRING);
  claimMap3.put("description", "total area");
  HashMap<String, Object> claimMap4 = new HashMap<String, Object>(2);
  claimMap4.put(JsonSchemaConstant.TYPE_KEY, JsonSchemaConstant.DATA_TYPE_STRING);
  claimMap4.put("description", "exclusive area");
  HashMap<String, Object> claimMap5 = new HashMap<String, Object>(2);
  claimMap5.put(JsonSchemaConstant.TYPE_KEY, JsonSchemaConstant.DATA_TYPE_STRING);
  claimMap5.put("description", "sharing area");
  HashMap<String, Object> claimMap6 = new HashMap<String, Object>(2);
  claimMap6.put(JsonSchemaConstant.TYPE_KEY, JsonSchemaConstant.DATA_TYPE_STRING);
  claimMap6.put("description", "use type");
  HashMap<String, Object> claimMap7 = new HashMap<String, Object>(2);
  claimMap7.put(JsonSchemaConstant.TYPE_KEY, JsonSchemaConstant.DATA_TYPE_STRING);
  claimMap7.put("description", "land number");
  HashMap<String, Object> claimMap8 = new HashMap<String, Object>(2);
  claimMap8.put(JsonSchemaConstant.TYPE_KEY, JsonSchemaConstant.DATA_TYPE_STRING);
  claimMap8.put("description", "holder");
  HashMap<String, Object> claimMap9 = new HashMap<String, Object>(2);
  claimMap9.put(JsonSchemaConstant.TYPE_KEY, JsonSchemaConstant.DATA_TYPE_STRING);
  claimMap9.put("description", "getting price");
  HashMap<String, Object> claimMap10 = new HashMap<String, Object>(2);
  claimMap10.put(JsonSchemaConstant.TYPE_KEY, JsonSchemaConstant.DATA_TYPE_STRING);
  claimMap10.put("description", "site");
  claim.put("certificateNumber", claimMap1);
  claim.put("landType", claimMap2);
  claim.put("totalArea", claimMap3);
  claim.put("exclusiveArea", claimMap4);
  claim.put("sharingArea", claimMap5);
  claim.put("useType", claimMap6);
  claim.put("landNumber", claimMap7);
  claim.put("holder", claimMap8);
  claim.put("gettingPrice", claimMap9);
  claim.put("site", claimMap10);
  
  propertites.put("claim", claim);
  ctCptMapArgs.setProperties(propertites);
  
  String[] required = { "landType", "totalArea" };
  ctCptMapArgs.setRequired(required);

  CTCptService ctCptService = new CTCptService();
  ResponseData<CptBaseInfo> response = ctCptService.updateCpt(ctCptMapArgs, 2000000);
```

- 返回示例

```
{
  "result": {
    "cptId": 2000000,
    "cptVersion": 2
  },
  "errorCode": 0,
  "errorMessage": "success",
  "transactionInfo": {
    "blockNumber": 778,
    "transactionHash": "0x103ede138aaaa25d86556bf4b82ffe701a69c2bade1d6b103bccae555a5f1f1d",
    "transactionIndex": 0
  }
}
```

### 4.5 查询CPT编号列表

#### 基本信息

```
接口名称: com.chinatrust.sdk.client.CTCptClient.getCptIdList
接口定义: ResponseData<List<Integer>> getCptIdList(Integer startPos, Integer num);
接口描述: 查询已注册的CPT编号列表
```

#### 传入参数

| 名称     | 类型    | 非空 | 说明              | 备注 |
| -------- | ------- | ---- | ----------------- | ---- |
| startPos | Integer | Y    | 起始位置（0开始） |      |
| num      | Integer | Y    | 条数              |      |

#### 返回参数

| 名称   | 类型          | 说明        | 备注 |
| ------ | ------------- | ----------- | ---- |
| result | List<Integer> | CPT编号列表 |      |

#### 调用示例

```java
  CTCptService ctCptService = new CTCptService();
  ResponseData<List<Integer>> cptIdList = ctCptClient.getCptIdList(0, 5);
```

- 返回示例

```
{
  "result": [
    2000000,
    2000001
  ],
  "errorCode": 0,
  "errorMessage": "success",
  "transactionInfo": null
}
```

### 6. 查询CPT数量

#### 基本信息

```
接口名称: com.chinatrust.sdk.client.CTCptClient.getCptCount
接口定义: ResponseData<Integer> getCptCount();
接口描述: 从链上的智能合约获取已经注册的CPT数量
```

#### 传入参数

#### 返回参数

| 名称   | 类型    | 说明    | 备注 |
| ------ | ------- | ------- | ---- |
| result | Integer | CPT数量 |      |

#### 调用示例

```java
  CTCptService ctCptService = new CTCptService();
  ResponseData<Integer> cptCount = ctCptClient.getCptCount();
```

- 返回示例

```
{
  "result": 5,
  "errorCode": 0,
  "errorMessage": "success",
  "transactionInfo": null
}
```

## 5. Credential相关接口

### 5.1 创建Credential
#### **基本信息**

```
接口名称: com.chinatrust.sdk.client.CTCredentialClient.createCredential
接口定义: ResponseData<CTCredentialPojo> createCredential(CTCreateCredentialPojoArgs args);
接口描述: 创建电子凭证
```

#### 传入参数

- com.chinatrust.sdk.client.CTCreateCredentialPojoArgs

| 名称               | 类型                | 非空 | 说明                                                | 备注 |
| ------------------ | ------------------- | ---- | --------------------------------------------------- | ---- |
| weIdAuthentication | WeIdAuthentication  | Y    | 认证信息，包含WeIdentity DID和私钥                  | 见下 |
| cptId              | String              | Y    | 指定的CPT ID                                        |      |
| expirationDate     | Long                | Y    | 到期时间                                            |      |
| claim              | Map<String, Object> | Y    | 创建凭证需要的claim数据                             |      |
| docType            | CredentialType      | Y    | 凭证类型enum，默认为Original，可选ZKP类型和Lite类型 |      |
| issuer             | Map<String, String> | Y    | 发行方信息                                          |      |
| recipient          | Map<String, String> | Y    | 接收方信息                                          |      |

- com.webank.weid.protocol.base.WeIdAuthentication

| 名称            | 类型           | 非空 | 说明                      | 备注                     |
| --------------- | -------------- | ---- | ------------------------- | ------------------------ |
| weId            | String         | Y    | CPT发布者的WeIdentity DID | WeIdentity DID的格式传入 |
| weIdPublicKeyId | String         | N    | 公钥Id                    |                          |
| weIdPrivateKey  | WeIdPrivateKey | Y    |                           | 交易私钥，见下           |

- com.webank.weid.protocol.base.WeIdPrivateKey

| 名称       | 类型   | 说明 | 备注               |
| ---------- | ------ | ---- | ------------------ |
| privateKey | String | 私钥 | 使用十进制数字表示 |

#### 返回参数

| 名称   | 类型             | 说明 | 备注           |
| ------ | ---------------- | ---- | -------------- |
| result | CTCredentialPojo |      | 凭证数据，见下 |

- com.chinatrust.sdk.client.CTCredentialPojo

| 名称         | 类型                | 说明           | 备注 |
| ------------ | ------------------- | -------------- | ---- |
| context      | String              |                |      |
| type         | String              |                |      |
| cptBaseInfo  | CptBaseInfo         | CPT基本信息    | 见下 |
| ctCptMapArgs | CTCptMapArgs        | CPT参数        | 见下 |
| metaData     | Cpt.MetaData        | CPT元数据      | 见下 |
| docType      | Map<String, Object> | 文档类型       |      |
| proof        | Map<String, Object> | 签名数据结构体 |      |

- com.webank.weid.protocol.base.CptBaseInfo

| 名称       | 类型    | 说明     | 备注 |
| ---------- | ------- | -------- | ---- |
| cptId      | Integer | cpId编号 |      |
| cptVersion | Integer | 版本号   |      |

- com.chinatrust.sdk.client.CTCptMapArgs

| 名称               | 类型                | 非空 | 说明                                      | 备注 |
| ------------------ | ------------------- | ---- | ----------------------------------------- | ---- |
| weIdAuthentication | WeIdAuthentication  | Y    | 认证信息，包含WeIdentity DID和私钥        |      |
| cptName            | String              | Y    | CPT名字                                   |      |
| cptDescription     | String              | Y    | CPT描述                                   |      |
| cptManagment       | String              | Y    | 权限管理合约地址                          |      |
| credentialType     | Map<String, Object> | Y    | 文档类型（可信文档/可流转文档）           |      |
| url                | String              | Y    | 查看或申请基于该CPT签发的credential的网址 |      |
| supervisable       | String              | Y    | 是否支持监管（true/false）                |      |
| properties         | Map<String, Object> | Y    | 必要属性                                  |      |
| required           | String[]            | Y    | 具体业务属性字段                          |      |

- com.webank.weid.protocol.base.Cpt.MetaData

| 名称         | 类型   | 说明                      | 备注                                      |
| ------------ | ------ | ------------------------- | ----------------------------------------- |
| cptPublisher | String | CPT发布者的WeIdentity DID | WeIdentity DID格式数据                    |
| cptSignature | String | 签名数据                  | cptPublisher与cptJsonSchema拼接的签名数据 |
| updated      | long   | 更新时间                  |                                           |
| created      | long   | 创建日期                  |                                           |

#### 调用示例

```java
  CTCreateCredentialPojoArgs ctCreateCredentialPojoArgs = new CTCreateCredentialPojoArgs();
  
  WeIdAuthentication weIdAuthentication = new WeIdAuthentication();
  weIdAuthentication.setWeId("did:weid:100:0xc204e875442db20b7316f726f0c1dccae7d39b3d");
  weIdAuthentication.setWeIdPrivateKey(new WeIdPrivateKey("7537941240201473300322488397004461834560927189780643296560422282794341761545"));
  weIdAuthentication.setWeIdPublicKeyId("did:weid:100:0xc204e875442db20b7316f726f0c1dccae7d39b3dkey-0");
  ctCreateCredentialPojoArgs.setWeIdAuthentication(weIdAuthentication);
  
  ctCreateCredentialPojoArgs.setCptId(2000000);
  ctCreateCredentialPojoArgs.setExpirationDate(System.currentTimeMillis() + 1000L * 60 * 60 * 24 * 365 * 100);
  
  Map<String, String> issuer = new HashMap<String, String>(2);
  issuer.put("WeID", "did:weid:100:0xc204e875442db20b7316f726f0c1dccae7d39b3d");
  issuer.put("name", "China Land Managment");
  ctCreateCredentialPojoArgs.setIssuer(issuer);
  
  Map<String, String> recipient = new HashMap<String, String>(2);
  recipient.put("WeID", "did:weid:100:0xc204e875442db20b7316f726f0c1dccae7d39b3d");
  recipient.put("name", "China Evergrande");
  ctCreateCredentialPojoArgs.setRecipient(recipient);
  
  HashMap<String, Object> claim = new HashMap<String, Object>(10);
  claim.put("certificateNumber", "123456");
  claim.put("landType", "commerce");
  claim.put("totalArea", "15568.26m^2");
  claim.put("exclusiveArea", "14000m^2");
  claim.put("sharingArea", "1568.26m^2");
  claim.put("useType", "selling");
  claim.put("landNumber", "56-20-12");
  claim.put("holder", "China Evergrande");
  claim.put("gettingPrice", "500,000,000RMB");
  claim.put("site", "ShaHe XiLu 001");
  ctCreateCredentialPojoArgs.setClaim(claim);

  CTCredentialClient ctCredentialClient = new CTCredentialService();
  ResponseData<CTCredentialPojo> response = ctCredentialClient.createCredential(ctCreateCredentialPojoArgs);
```

- 返回示例

```
{
  "result": {
    "type": "object",
    "cptBaseInfo": {
      "cptId": 2000000,
      "cptVersion": 2
    },
    "ctCptMapArgs": {
      "weIdAuthentication": null,
      "cptType": "ORIGINAL",
      "cptName": "landcertificate",
      "cptDescription": "unit land certificate",
      "cptManagment": "0x57bc072ab8af771c0978f94bd3c9c76ad0cc6eff",
      "credentialType": {
        "cptTradeToken": "0x45d617c5157e9a07ac570d18692d362c3e50c852"
      },
      "url": "http://chinatrust.com",
      "supervisable": "false",
      "properties": null,
      "required": [
        "landType",
        "totalArea"
      ]
    },
    "metaData": {
      "cptPublisher": "did:weid:100:0xc204e875442db20b7316f726f0c1dccae7d39b3d",
      "cptSignature": "ABnVA5FTH2AqfAOzL41TgTf2bra4Enm7idz+BxsjhXMSbfCwvK4VWuUYIwcVKnyKdRdcMAj1pYIC1TLXy96Ax+E=",
      "created": 1637221901,
      "updated": 0
    },
    "properties": {
      "recipient": {
        "WeID": "did:weid:100:0x05485b6415dfbcaa370a5b2724646e97ff7a7ac2",
        "name": "China Evergrande"
      },
      "claim": {
        "exclusiveArea": "14000m^2",
        "site": "ShaHe XiLu 001",
        "gettingPrice": "500,000,000RMB",
        "landNumber": "56-20-12",
        "certificateNumber": "123456",
        "holder": "China Evergrande",
        "landType": "commerce",
        "useType": "selling",
        "sharingArea": "1568.26m^2",
        "totalArea": "15568.26m^2"
      },
      "id": "92672185-537c-4202-855c-9f09cd1718b5",
      "issuanceDate": 1637914178,
      "issuer": {
        "WeID": "did:weid:100:0x6e97e5ba180a78cbf8f31ff4925d5072ceeb0c26",
        "name": "China Land Managment"
      },
      "expirationDate": 4791514178
    },
    "context": "https://github.com/WeBankFinTech/WeIdentity/blob/master/context/v1",
    "docType": [
      "VerifiableCredential",
      "original"
    ],
    "proof": {
      "creator": "did:weid:100:0x6e97e5ba180a78cbf8f31ff4925d5072ceeb0c26key-0",
      "salt": {
        "exclusiveArea": "keyUc",
        "site": "eu82d",
        "gettingPrice": "17IFU",
        "landNumber": "0JMFx",
        "certificateNumber": "15hLS",
        "holder": "MXnZp",
        "landType": "CSe7c",
        "useType": "qJKZR",
        "sharingArea": "KESVH",
        "totalArea": "TyBZu"
      },
      "created": 1637914178,
      "type": "Secp256k1",
      "signatureValue": "hwarGfLTvnRH84Vk2AsXGVlkubsZjcaOt1/v0xe0EXMczB4Jjze0AEzOpG4CkOEC554qts0G3vaZHdqMYvjtkwE="
    },
    "claim": {
      "exclusiveArea": "14000m^2",
      "site": "ShaHe XiLu 001",
      "gettingPrice": "500,000,000RMB",
      "landNumber": "56-20-12",
      "certificateNumber": "123456",
      "holder": "China Evergrande",
      "landType": "commerce",
      "useType": "selling",
      "sharingArea": "1568.26m^2",
      "totalArea": "15568.26m^2"
    },
    "signatureTargetHash": "0xb1b71153dd272aa394fe97dbb199ff1bf34e71c2a3f00e8d0bc40feb6d00b496",
    "signatureTargetData": "{\"claim\":{\"certificateNumber\":\"0xa49136be47796810e1030da9dfeb47a813be214e7e165eed0001b914237a5a2b\",\"exclusiveArea\":\"0x672b94c36277ca7d09c72fdda499867269d5f7166aafc24de80c218d98b78bba\",\"gettingPrice\":\"0x3468577a9b4ad78c8c83910d41d3ca492524c9e4c1ea582be7956389ff4385ea\",\"holder\":\"0x735e66ee1d086be1e505520c0d62f14ab280d84ef539770b2ac0ea6897f05fd0\",\"landNumber\":\"0xfc01b0a9fab9d97e11984782fe2bb1d33f530aca42c32524195ada254ae88b17\",\"landType\":\"0xa04c935b87ef63a28120558b3f1dda3ce352c6eb0c16b605a2e4fd7e21acc748\",\"sharingArea\":\"0x2381f1dacaf54bf2a2efe5f7018efae6a25e66fb7874101c738cda2a297a1452\",\"site\":\"0xf199ade9e75fc4c6af99a675963b8cbb56ceba8b0660828a246e7e4aeb0031c0\",\"totalArea\":\"0x4f4ee16745e35155c80dac82be5cf925153ad08b3fbe67b803f8c36e16770052\",\"useType\":\"0xcb80dd8fbde12438ebb2d7b7f96f70cce80159bea649f901c51597f0fb9cc903\"},\"context\":\"https://github.com/WeBankFinTech/WeIdentity/blob/master/context/v1\",\"cptId\":2000000,\"expirationDate\":4791514178,\"id\":\"92672185-537c-4202-855c-9f09cd1718b5\",\"issuanceDate\":1637914178,\"issuer\":\"did:weid:100:0x6e97e5ba180a78cbf8f31ff4925d5072ceeb0c26\",\"type\":[\"VerifiableCredential\",\"original\"]}",
    "salt": {
      "exclusiveArea": "keyUc",
      "site": "eu82d",
      "gettingPrice": "17IFU",
      "landNumber": "0JMFx",
      "certificateNumber": "15hLS",
      "holder": "MXnZp",
      "landType": "CSe7c",
      "useType": "qJKZR",
      "sharingArea": "KESVH",
      "totalArea": "TyBZu"
    }
  },
  "errorCode": 0,
  "errorMessage": "success",
  "transactionInfo": null
}
```

### 5.2 获取Credential的哈希值

#### 基本信息

```
接口名称: com.chinatrust.sdk.client.CTCredentialClient.getCredentialPojoHash
接口定义: ResponseData<String> getCredentialPojoHash(CTCredentialPojo ctCredentialPojo);
接口描述: 获取一个Credential的整体哈希值（不包括Credential中的proof里的salt字段），在生成Evidence时调用
```

#### 传入参数

- com.chinatrust.sdk.client.CTCredentialPojo

| 名称             | 类型             | 非空 | 说明     | 备注              |
| ---------------- | ---------------- | ---- | -------- | ----------------- |
| ctCredentialPojo | CTCredentialPojo | Y    | 凭证信息 | 见5.1接口返回参数 |

#### 返回参数

| 名称   | 类型   | 说明   | 备注 |
| ------ | ------ | ------ | ---- |
| result | String | 哈希值 |      |

#### 调用示例

```java
  CTCreateCredentialPojoArgs ctCreateCredentialPojoArgs = new CTCreateCredentialPojoArgs();
  
  WeIdAuthentication weIdAuthentication = new WeIdAuthentication();
  weIdAuthentication.setWeId("did:weid:100:0xc204e875442db20b7316f726f0c1dccae7d39b3d");
  weIdAuthentication.setWeIdPrivateKey(new WeIdPrivateKey("7537941240201473300322488397004461834560927189780643296560422282794341761545"));
  weIdAuthentication.setWeIdPublicKeyId("did:weid:100:0xc204e875442db20b7316f726f0c1dccae7d39b3dkey-0");
  ctCreateCredentialPojoArgs.setWeIdAuthentication(weIdAuthentication);
  
  ctCreateCredentialPojoArgs.setCptId(2000000);
  ctCreateCredentialPojoArgs.setExpirationDate(System.currentTimeMillis() + 1000L * 60 * 60 * 24 * 365 * 100);
  
  Map<String, String> issuer = new HashMap<String, String>(2);
  issuer.put("WeID", "did:weid:100:0xc204e875442db20b7316f726f0c1dccae7d39b3d");
  issuer.put("name", "China Land Managment");
  ctCreateCredentialPojoArgs.setIssuer(issuer);
  
  Map<String, String> recipient = new HashMap<String, String>(2);
  recipient.put("WeID", "did:weid:100:0xc204e875442db20b7316f726f0c1dccae7d39b3d");
  recipient.put("name", "China Evergrande");
  ctCreateCredentialPojoArgs.setRecipient(recipient);
  
  HashMap<String, Object> claim = new HashMap<String, Object>(10);
  claim.put("certificateNumber", "123456");
  claim.put("landType", "commerce");
  claim.put("totalArea", "15568.26m^2");
  claim.put("exclusiveArea", "14000m^2");
  claim.put("sharingArea", "1568.26m^2");
  claim.put("useType", "selling");
  claim.put("landNumber", "56-20-12");
  claim.put("holder", "China Evergrande");
  claim.put("gettingPrice", "500,000,000RMB");
  claim.put("site", "ShaHe XiLu 001");
  ctCreateCredentialPojoArgs.setClaim(claim);

  CTCredentialClient ctCredentialClient = new CTCredentialService();
  ResponseData<CTCredentialPojo> response = ctCredentialClient.createCredential(ctCreateCredentialPojoArgs);

  ResponseData<String> responseHash = ctCredentialClient.getCredentialPojoHash(response.getResult());
```

- 返回示例

```
{
  "result": "0xd1264870577c09207b876cf376db22d6095e526aa82848a03e9e5198ec36f86d",
  "errorCode": 0,
  "errorMessage": "success",
  "transactionInfo": null
}
```

### 5.3 通过WeId验证Credential

#### 基本信息

```
接口名称: com.chinatrust.sdk.client.CTCredentialClient.verify
接口定义: ResponseData<Boolean> verify(String issuerWeId, CTCredentialPojo ctCredentialPojo);
接口描述: 根据签发者的WeId验证Credential
```

#### 传入参数

- com.chinatrust.sdk.client.CTCredentialPojo

| 名称             | 类型             | 非空 | 说明      | 备注              |
| ---------------- | ---------------- | ---- | --------- | ----------------- |
| ctCredentialPojo | CTCredentialPojo | Y    | 凭证信息  | 见5.1接口返回参数 |
| issuerWeId       | String           | Y    | 发行方DID |                   |

#### 返回参数

| 名称   | 类型    | 说明         | 备注 |
| ------ | ------- | ------------ | ---- |
| result | Boolean | 验证是否成功 |      |

#### 调用示例

```java
  CTCreateCredentialPojoArgs ctCreateCredentialPojoArgs = new CTCreateCredentialPojoArgs();
  
  WeIdAuthentication weIdAuthentication = new WeIdAuthentication();
  weIdAuthentication.setWeId("did:weid:100:0xc204e875442db20b7316f726f0c1dccae7d39b3d");
  weIdAuthentication.setWeIdPrivateKey(new WeIdPrivateKey("7537941240201473300322488397004461834560927189780643296560422282794341761545"));
  weIdAuthentication.setWeIdPublicKeyId("did:weid:100:0xc204e875442db20b7316f726f0c1dccae7d39b3dkey-0");
  ctCreateCredentialPojoArgs.setWeIdAuthentication(weIdAuthentication);
  
  ctCreateCredentialPojoArgs.setCptId(2000000);
  ctCreateCredentialPojoArgs.setExpirationDate(System.currentTimeMillis() + 1000L * 60 * 60 * 24 * 365 * 100);
  
  Map<String, String> issuer = new HashMap<String, String>(2);
  issuer.put("WeID", "did:weid:100:0xc204e875442db20b7316f726f0c1dccae7d39b3d");
  issuer.put("name", "China Land Managment");
  ctCreateCredentialPojoArgs.setIssuer(issuer);
  
  Map<String, String> recipient = new HashMap<String, String>(2);
  recipient.put("WeID", "did:weid:100:0xc204e875442db20b7316f726f0c1dccae7d39b3d");
  recipient.put("name", "China Evergrande");
  ctCreateCredentialPojoArgs.setRecipient(recipient);
  
  HashMap<String, Object> claim = new HashMap<String, Object>(10);
  claim.put("certificateNumber", "123456");
  claim.put("landType", "commerce");
  claim.put("totalArea", "15568.26m^2");
  claim.put("exclusiveArea", "14000m^2");
  claim.put("sharingArea", "1568.26m^2");
  claim.put("useType", "selling");
  claim.put("landNumber", "56-20-12");
  claim.put("holder", "China Evergrande");
  claim.put("gettingPrice", "500,000,000RMB");
  claim.put("site", "ShaHe XiLu 001");
  ctCreateCredentialPojoArgs.setClaim(claim);

  CTCredentialClient ctCredentialClient = new CTCredentialService();
  ResponseData<CTCredentialPojo> response = ctCredentialClient.createCredential(ctCreateCredentialPojoArgs);

  ResponseData<Boolean> responseVerify = ctCredentialClient.verify("did:weid:100:0xc204e875442db20b7316f726f0c1dccae7d39b3d", response.getResult());
```

- 返回示例

```
{
  "result": true,
  "errorCode": 0,
  "errorMessage": "success",
  "transactionInfo": null
}
```

### 5.4 通过WeIdPublicKey验证Credential

#### 基本信息

```
接口名称: com.chinatrust.sdk.client.CTCredentialClient.verify
接口定义: ResponseData<Boolean> verify(WeIdPublicKey issuerPublicKey, CTCredentialPojo ctCredentialPojo);
接口描述: 根据签发者的WeIdPublicKey验证Credential
```

#### 传入参数

- com.chinatrust.sdk.client.CTCredentialPojo

| 名称             | 类型             | 非空 | 说明       | 备注              |
| ---------------- | ---------------- | ---- | ---------- | ----------------- |
| ctCredentialPojo | CTCredentialPojo | Y    | 凭证信息   | 见5.1接口返回参数 |
| issuerPublicKey  | WeIdPublicKey    | Y    | 发行方公钥 | 见下              |

- com.webank.weid.protocol.base.WeIdPublicKey

| 名称      | 类型   | 非空 | 说明 | 备注 |
| --------- | ------ | ---- | ---- | ---- |
| publicKey | String | Y    | 公钥 |      |

#### 返回参数

| 名称   | 类型    | 说明         | 备注 |
| ------ | ------- | ------------ | ---- |
| result | Boolean | 验证是否成功 |      |

#### 调用示例

```java
  CTCreateCredentialPojoArgs ctCreateCredentialPojoArgs = new CTCreateCredentialPojoArgs();
  
  WeIdAuthentication weIdAuthentication = new WeIdAuthentication();
  weIdAuthentication.setWeId("did:weid:100:0xc204e875442db20b7316f726f0c1dccae7d39b3d");
  weIdAuthentication.setWeIdPrivateKey(new WeIdPrivateKey("7537941240201473300322488397004461834560927189780643296560422282794341761545"));
  weIdAuthentication.setWeIdPublicKeyId("did:weid:100:0xc204e875442db20b7316f726f0c1dccae7d39b3dkey-0");
  ctCreateCredentialPojoArgs.setWeIdAuthentication(weIdAuthentication);
  
  ctCreateCredentialPojoArgs.setCptId(2000000);
  ctCreateCredentialPojoArgs.setExpirationDate(System.currentTimeMillis() + 1000L * 60 * 60 * 24 * 365 * 100);
  
  Map<String, String> issuer = new HashMap<String, String>(2);
  issuer.put("WeID", "did:weid:100:0xc204e875442db20b7316f726f0c1dccae7d39b3d");
  issuer.put("name", "China Land Managment");
  ctCreateCredentialPojoArgs.setIssuer(issuer);
  
  Map<String, String> recipient = new HashMap<String, String>(2);
  recipient.put("WeID", "did:weid:100:0xc204e875442db20b7316f726f0c1dccae7d39b3d");
  recipient.put("name", "China Evergrande");
  ctCreateCredentialPojoArgs.setRecipient(recipient);
  
  HashMap<String, Object> claim = new HashMap<String, Object>(10);
  claim.put("certificateNumber", "123456");
  claim.put("landType", "commerce");
  claim.put("totalArea", "15568.26m^2");
  claim.put("exclusiveArea", "14000m^2");
  claim.put("sharingArea", "1568.26m^2");
  claim.put("useType", "selling");
  claim.put("landNumber", "56-20-12");
  claim.put("holder", "China Evergrande");
  claim.put("gettingPrice", "500,000,000RMB");
  claim.put("site", "ShaHe XiLu 001");
  ctCreateCredentialPojoArgs.setClaim(claim);

  CTCredentialClient ctCredentialClient = new CTCredentialService();
  ResponseData<CTCredentialPojo> response = ctCredentialClient.createCredential(ctCreateCredentialPojoArgs);

  WeIdPublicKey weIdPublicKey = new WeIdPublicKey();
weIdPublicKey.setPublicKey("4432445553701865582083821298682786108232857406963365256782233667063089026603279356140927749891030623113144876717056777356676658456163011484717338048459954");

  ResponseData<Boolean> responseVerify = ctCredentialClient.verify(weIdPublicKey, response.getResult());
```

- 返回示例

```
{
  "result": true,
  "errorCode": 0,
  "errorMessage": "success",
  "transactionInfo": null
}
```

### 5.5 创建选择性披露Credential

#### 基本信息

```
接口名称: com.chinatrust.sdk.client.CTCredentialClient.verify
接口定义: ResponseData<CTCredentialPojo> createSelectiveCredential(CTCredentialPojo ctCredentialPojo, ClaimPolicy claimPolicy);
接口描述: 根据自定义披露策略对全披露的凭证生成选择性披露凭证
```

#### 传入参数

- com.chinatrust.sdk.client.CTCredentialPojo

| 名称             | 类型             | 非空 | 说明     | 备注              |
| ---------------- | ---------------- | ---- | -------- | ----------------- |
| ctCredentialPojo | CTCredentialPojo | Y    | 凭证信息 | 见5.1接口返回参数 |
| claimPolicy      | ClaimPolicy      | Y    | 披露策略 | 见下              |

- com.webank.weid.protocol.base.ClaimPolicy

| 名称                | 类型   | 非空 | 说明     | 备注                                                         |
| ------------------- | ------ | ---- | -------- | ------------------------------------------------------------ |
| fieldsToBeDisclosed | String | Y    | 披露配置 | 根据claim匹配的结构，为一个Json字符串，和Claim字段格式匹配。 |

#### 返回参数

| 名称   | 类型             | 说明               | 备注              |
| ------ | ---------------- | ------------------ | ----------------- |
| result | CTCredentialPojo | 选择性披露凭证信息 | 见5.1接口返回参数 |

#### 调用示例

```java
  CTCreateCredentialPojoArgs ctCreateCredentialPojoArgs = new CTCreateCredentialPojoArgs();
  
  WeIdAuthentication weIdAuthentication = new WeIdAuthentication();
  weIdAuthentication.setWeId("did:weid:100:0xc204e875442db20b7316f726f0c1dccae7d39b3d");
  weIdAuthentication.setWeIdPrivateKey(new WeIdPrivateKey("7537941240201473300322488397004461834560927189780643296560422282794341761545"));
  weIdAuthentication.setWeIdPublicKeyId("did:weid:100:0xc204e875442db20b7316f726f0c1dccae7d39b3dkey-0");
  ctCreateCredentialPojoArgs.setWeIdAuthentication(weIdAuthentication);
  
  ctCreateCredentialPojoArgs.setCptId(2000000);
  ctCreateCredentialPojoArgs.setExpirationDate(System.currentTimeMillis() + 1000L * 60 * 60 * 24 * 365 * 100);
  
  Map<String, String> issuer = new HashMap<String, String>(2);
  issuer.put("WeID", "did:weid:100:0xc204e875442db20b7316f726f0c1dccae7d39b3d");
  issuer.put("name", "China Land Managment");
  ctCreateCredentialPojoArgs.setIssuer(issuer);
  
  Map<String, String> recipient = new HashMap<String, String>(2);
  recipient.put("WeID", "did:weid:100:0xc204e875442db20b7316f726f0c1dccae7d39b3d");
  recipient.put("name", "China Evergrande");
  ctCreateCredentialPojoArgs.setRecipient(recipient);
  
  HashMap<String, Object> claim = new HashMap<String, Object>(10);
  claim.put("certificateNumber", "123456");
  claim.put("landType", "commerce");
  claim.put("totalArea", "15568.26m^2");
  claim.put("exclusiveArea", "14000m^2");
  claim.put("sharingArea", "1568.26m^2");
  claim.put("useType", "selling");
  claim.put("landNumber", "56-20-12");
  claim.put("holder", "China Evergrande");
  claim.put("gettingPrice", "500,000,000RMB");
  claim.put("site", "ShaHe XiLu 001");
  ctCreateCredentialPojoArgs.setClaim(claim);

  CTCredentialClient ctCredentialClient = new CTCredentialService();
  ResponseData<CTCredentialPojo> response = ctCredentialClient.createCredential(ctCreateCredentialPojoArgs);

  // 选择性披露
  ClaimPolicy claimPolicy = new ClaimPolicy();
  claimPolicy.setFieldsToBeDisclosed("{\"landNumber\":1,\"totalArea\":1,\"gettingPrice\":1}");
  ResponseData<CTCredentialPojo> responseSelective = ctCredentialClient.createSelectiveCredential(response.getResult(), claimPolicy);
```

- 返回示例

```
{
  "result": {
    "type": "object",
    "cptBaseInfo": {
      "cptId": 2000000,
      "cptVersion": 1
    },
    "ctCptMapArgs": {
      "weIdAuthentication": {
        "weId": null,
        "weIdPublicKeyId": null,
        "weIdPrivateKey": null
      },
      "cptType": "ORIGINAL",
      "cptName": "landcertificate",
      "cptDescription": "unit land certificate",
      "cptManagment": "0x57bc072ab8af771c0978f94bd3c9c76ad0cc6eff",
      "credentialType": {
        "cptTradeToken": "0x45d617c5157e9a07ac570d18692d362c3e50c852"
      },
      "url": "http://chinatrust.com",
      "supervisable": "false",
      "properties": {},
      "required": [
        "landType",
        "totalArea"
      ]
    },
    "metaData": {
      "cptPublisher": "did:weid:100:0x1cd7fa8518c82d5b87fd5a150930a30471b58906",
      "cptSignature": "ATdzmrl6hLaKW3RH/peDNiXnJolMQbCvPTt7PVklWPpnc5ZlGcUA3S08mbZG8aEvST6gLKBhPRQ5DpM/rdZbOmM=",
      "created": 1636710016,
      "updated": 0
    },
    "properties": {
      "recipient": {
        "WeID": "did:weid:100:0x05485b6415dfbcaa370a5b2724646e97ff7a7ac2",
        "name": "China Evergrande"
      },
      "claim": {
        "exclusiveArea": "0xef416695a6311783374b0852b164789ccc6f48b0ff7fd8e90a34a4e8d8ff91e3",
        "site": "0xa29e050f4eec60707d4c6737f8724e81a92819ca85407cc88f708f40e4ca85c3",
        "gettingPrice": "500,000,000RMB",
        "landNumber": "56-20-12",
        "certificateNumber": "0x60a07b2aa63f5a8a366325c8ca787c2be1145f75240088c46369a8b0987f9c3a",
        "holder": "0xb216a35ca939fc8e74701e15712cd5089c7345dfc1e3501d6e87d14761409d58",
        "landType": "0x0d40c06332b670ee212bedcfe4bd011d45e75244c6b3b1e3ed16425cc53b8e54",
        "useType": "0x3ab925cfe6ffca2135c9232e571d5bf61216d2e3889fff1eb09bceeecce72abe",
        "sharingArea": "0x3a28419cb77ed27e6248fcf28ddf81cdea7a550f59c48303e916b53ef7bff346",
        "totalArea": "15568.26m^2"
      },
      "id": "1dc6d2b1-a7f6-4eed-82d0-af31363efe7c",
      "issuanceDate": 1636714893,
      "issuer": {
        "WeID": "did:weid:100:0x6e97e5ba180a78cbf8f31ff4925d5072ceeb0c26",
        "name": "China Land Managment"
      },
      "expirationDate": 4790314893
    },
    "context": "https://github.com/WeBankFinTech/WeIdentity/blob/master/context/v1",
    "docType": [
      "VerifiableCredential",
      "original"
    ],
    "proof": {
      "created": 1636714893,
      "creator": "did:weid:100:0x6e97e5ba180a78cbf8f31ff4925d5072ceeb0c26key-0",
      "salt": {
        "certificateNumber": "xaIml",
        "exclusiveArea": "4sHra",
        "gettingPrice": "6B4kI",
        "holder": "Ys7eO",
        "landNumber": "DDoNN",
        "landType": "jPWy9",
        "sharingArea": "9ieHs",
        "site": "AHu5c",
        "totalArea": "rjIpJ",
        "useType": "Re4fL"
      },
      "signatureValue": "zTiHgyHMWf6kQw+6sg/OGTiinb4/3vTb0zfCGToDrqVN6vAbkS2AnirAhW74GbxeR6cikyMY16pbq/gNxIRwqwA=",
      "type": "Secp256k1"
    },
    "claim": {
      "exclusiveArea": "0xef416695a6311783374b0852b164789ccc6f48b0ff7fd8e90a34a4e8d8ff91e3",
      "site": "0xa29e050f4eec60707d4c6737f8724e81a92819ca85407cc88f708f40e4ca85c3",
      "gettingPrice": "500,000,000RMB",
      "landNumber": "56-20-12",
      "certificateNumber": "0x60a07b2aa63f5a8a366325c8ca787c2be1145f75240088c46369a8b0987f9c3a",
      "holder": "0xb216a35ca939fc8e74701e15712cd5089c7345dfc1e3501d6e87d14761409d58",
      "landType": "0x0d40c06332b670ee212bedcfe4bd011d45e75244c6b3b1e3ed16425cc53b8e54",
      "useType": "0x3ab925cfe6ffca2135c9232e571d5bf61216d2e3889fff1eb09bceeecce72abe",
      "sharingArea": "0x3a28419cb77ed27e6248fcf28ddf81cdea7a550f59c48303e916b53ef7bff346",
      "totalArea": "15568.26m^2"
    },
    "signatureTargetHash": "0x60f32040c260a55e05e60701428eb5d3deb6e5cb9b44defc49d635ebb62267c7",
    "signatureTargetData": "{\"claim\":{\"certificateNumber\":\"0xdad02769e9b8af7d1748354ff65b3fa1c12110a43acc3ce72f97639a2c5ab9e5\",\"exclusiveArea\":\"0x46d24e2bc5cc23425813174adc629c40443ecf1f2e32e8154c8d7def06a4fa22\",\"gettingPrice\":\"0x98177b926d57b7c977db614adf98586e69851c0023c919f46bb4e68f6b7009a2\",\"holder\":\"0xefedf50e24e873e4cad2ea877b350d79beabf47773cb6b41e6008e951235ae7d\",\"landNumber\":\"0x65b82c69df27467367deca994548d12be20fc2a0ef7d36cfd737674ea2c041ea\",\"landType\":\"0x510ff3c8f4f3e6e03a00ad80dfddd36631142791e5d2c45112a140d6ecc640b0\",\"sharingArea\":\"0x3efbfbf732ed8c83426a099b6e13d8a93a4716f439341d387e614ed1eb2643c4\",\"site\":\"0x2d8d7e732e93188d7c470dfd4d9e4055212e3bb2487b021a1f980204bc082a5f\",\"totalArea\":\"0xe6b440529781d5a0f43313d64efc34eeac16a1bc91eb634b48e38a1dc81c5507\",\"useType\":\"0x25067bd007e0647542d6bf06717e9ac7edf2b1bbe9745aebd899eb4b68143ccc\"},\"context\":\"https://github.com/WeBankFinTech/WeIdentity/blob/master/context/v1\",\"cptId\":2000000,\"expirationDate\":4790314893,\"id\":\"1dc6d2b1-a7f6-4eed-82d0-af31363efe7c\",\"issuanceDate\":1636714893,\"issuer\":\"did:weid:100:0x6e97e5ba180a78cbf8f31ff4925d5072ceeb0c26\",\"type\":[\"VerifiableCredential\",\"original\"]}",
    "salt": {
      "exclusiveArea": "4sHra",
      "site": "AHu5c",
      "gettingPrice": "6B4kI",
      "landNumber": "DDoNN",
      "certificateNumber": "xaIml",
      "holder": "Ys7eO",
      "landType": "jPWy9",
      "useType": "Re4fL",
      "sharingArea": "9ieHs",
      "totalArea": "rjIpJ"
    }
  },
  "errorCode": 0,
  "errorMessage": "success",
  "transactionInfo": null
}
```