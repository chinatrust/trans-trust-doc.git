# 接口文档

## 1 工具接口

### 1.1 生成公私钥对

#### 传输协议规范

- 网络传输协议：使用HTTP协议
- 请求地址：**/step1/getKeyPair**
- 请求方式：POST
- 返回格式：JSON

#### 请求参数

***1）入参表***

***2）入参示例***

```
http://127.0.0.1:6101/chinatrust-demo/step1/getKeyPair
```

#### 返回参数 

***1）出参表***

| 序号 | 输出参数     | 类型   | 可为空 | 备注                       |
| ---- | ------------ | ------ | ------ | -------------------------- |
| 1    | errorCode    | int    | 否     | 返回码，0：成功 其它：失败 |
| 2    | errorMessage | String | 否     | 描述                       |
| 3    | result       | Object | 是     | 信息对象                   |

***2）出参示例***

```
{
  "result": {
    "publicKey": "4432445553701865582083821298682786108232857406963365256782233667063089026603279356140927749891030623113144876717056777356676658456163011484717338048459954",
    "privateKey": "7537941240201473300322488397004461834560927189780643296560422282794341761545"
  },
  "errorCode": 0,
  "errorMessage": "success",
  "transactionInfo": null
}
```

### 2.2 通过私钥查询公钥

#### 传输协议规范

- 网络传输协议：使用HTTP协议
- 请求地址：**/step2/getPublicKey?privateKey=**
- 请求方式：POST
- 返回格式：JSON

#### 请求参数

***1）入参表***

| 序号 | 输入参数   | 类型   | 可为空 | 备注 |
| ---- | ---------- | ------ | ------ | ---- |
| 1    | privateKey | String | 否     | 私钥 |

***2）入参示例***

```
http://127.0.0.1:6101/chinatrust-demo/step2/getPublicKey?privateKey=7537941240201473300322488397004461834560927189780643296560422282794341761545
```

#### 返回参数 

***1）出参表***

| 序号 | 输出参数     | 类型   | 可为空 | 备注                       |
| ---- | ------------ | ------ | ------ | -------------------------- |
| 1    | errorCode    | int    | 否     | 返回码，0：成功 其它：失败 |
| 2    | errorMessage | String | 否     | 描述                       |
| 3    | result       | Object | 是     | 信息对象                   |

***2）出参示例***

```
{
  "result": "4432445553701865582083821298682786108232857406963365256782233667063089026603279356140927749891030623113144876717056777356676658456163011484717338048459954",
  "errorCode": 0,
  "errorMessage": "success",
  "transactionInfo": null
}
```

## 2 初始化接口

### 2.1 初始化接口

部署WeID合约（重复部署则启用最新部署的地址）。

#### 传输协议规范

- 网络传输协议：使用HTTP协议
- 请求地址：**/ctInit**
- 请求方式：POST
- 返回格式：JSON

#### 请求参数

***1）入参表***

| 序号 | 输入参数   | 类型   | 可为空 | 备注   |
| ---- | ---------- | ------ | ------ | ------ |
| 1    | chainId    | String | 否     | 链编号 |
| 2    | privateKey | String | 否     | 私钥   |

***2）入参示例***

```
http://127.0.0.1:6101/chinatrust-demo/ctInit?chainId=100&privateKey=97853131880531688443535330677898891093870336824192132968066636298855968583697
```

#### 返回参数 

***1）出参表***

***2）出参示例***

## 3 DID接口

### 3.1 创建DID

不包含参数，自动生成公私钥并返回。

#### 传输协议规范

- 网络传输协议：使用HTTP协议
- 请求地址：**/createWeId**
- 请求方式：POST
- 返回格式：JSON

#### 请求参数

***1）入参表***

***2）入参示例***

```
http://127.0.0.1:6101/chinatrust-demo/createWeId
```

#### 返回参数 

***1）出参表***

| 序号 | 输出参数     | 类型   | 可为空 | 备注                       |
| ---- | ------------ | ------ | ------ | -------------------------- |
| 1    | errorCode    | int    | 否     | 返回码，0：成功 其它：失败 |
| 2    | errorMessage | String | 否     | 描述                       |
| 3    | result       | Object | 是     | 信息对象                   |

***2）出参示例***

```
{
  "result": {
    "weId": "did:weid:100:0x3fb59dc4daa45790ccd2290f854a7d25e1954c52",
    "userWeIdPublicKey": {
      "publicKey": "11730475294836178996157792334384365646975508238599542693476044195774594983460723599299778564328269091983032189467368487954000575514359315884319901166506666"
    },
    "userWeIdPrivateKey": {
      "privateKey": "95376921965511027783320490167708753595638938146685900560917387346870565391816"
    }
  },
  "errorCode": 0,
  "errorMessage": "success",
  "transactionInfo": {
    "blockNumber": 775,
    "transactionHash": "0xc2aed4f7372c0bde53d3a7b472d03788cf3e23224fc52bc38c57ae69d61bd4c7",
    "transactionIndex": 0
  }
}
```

### 3.2 根据公私钥创建DID

#### 传输协议规范

- 网络传输协议：使用HTTP协议
- 请求地址：**/createWeIdArgs?privateKey=&publicKey=**
- 请求方式：POST
- 返回格式：JSON

#### 请求参数

***1）入参表***

| 序号 | 输入参数   | 类型   | 可为空 | 备注 |
| ---- | ---------- | ------ | ------ | ---- |
| 1    | privateKey | String | 否     | 私钥 |
| 2    | publicKey  | String | 否     | 公钥 |

***2）入参示例***

```
http://127.0.0.1:6101/chinatrust-demo/createWeIdArgs?privateKey=7537941240201473300322488397004461834560927189780643296560422282794341761545&publicKey=4432445553701865582083821298682786108232857406963365256782233667063089026603279356140927749891030623113144876717056777356676658456163011484717338048459954
```

#### 返回参数 

***1）出参表***

| 序号 | 输出参数     | 类型   | 可为空 | 备注                       |
| ---- | ------------ | ------ | ------ | -------------------------- |
| 1    | errorCode    | int    | 否     | 返回码，0：成功 其它：失败 |
| 2    | errorMessage | String | 否     | 描述                       |
| 3    | result       | Object | 是     | 信息对象                   |

***2）出参示例***

```
{
  "result": "did:weid:100:0xc204e875442db20b7316f726f0c1dccae7d39b3d",
  "errorCode": 0,
  "errorMessage": "success",
  "transactionInfo": {
    "blockNumber": 777,
    "transactionHash": "0x4acf166720b38e6a05952dbe1e63b0d3c5ea6429c53765f40d1001fa9b015133",
    "transactionIndex": 0
  }
}
```

## 4 CPT接口

### 4.1 注册CPT

cptId为空时自动创建，传入则指定。

#### 传输协议规范

- 网络传输协议：使用HTTP协议
- 请求地址：**/step1/registerCpt**
- 请求方式：POST
- 返回格式：JSON

#### 请求参数

***1）入参表***

| 序号 | 输入参数       | 类型                | 可为空 | 备注                                      |
| ---- | -------------- | ------------------- | ------ | ----------------------------------------- |
| 1    | privateKey     | String              | 否     | 私钥                                      |
| 2    | weId           | String              | 否     | 发布者weId                                |
| 3    | cptId          | Integer             | 是     | 指定的CPT ID                              |
| 4    | cptName        | String              | 否     | CPT名字                                   |
| 5    | cptDescription | String              | 否     | CPT描述                                   |
| 6    | cptManagment   | String              | 否     | 权限管理合约地址                          |
| 7    | credentialType | Map<String, Object> | 否     | 文档类型（可信文档/可流转文档）           |
| 8    | url            | String              | 否     | 查看或申请基于该CPT签发的credential的网址 |
| 9    | supervisable   | String              | 否     | 是否支持监管（true/false）                |
| 10   | required       | String[]            | 否     | 必要属性                                  |
| 11   | properties     | Map<String, Object> | 否     | 具体业务属性字段                          |

***2）入参示例***

```
http://127.0.0.1:6101/chinatrust-demo/step1/registerCpt
```

```json
{
  "cptDescription": "unit land certificate",
  "cptId": 2000000,
  "cptManagment": "0x57bc072ab8af771c0978f94bd3c9c76ad0cc6eff",
  "cptName": "landcertificate",
  "credentialType": {
    "cptTradeToken": "0x45d617c5157e9a07ac570d18692d362c3e50c852"
  },
  "privateKey": "7537941240201473300322488397004461834560927189780643296560422282794341761545",
  "properties": {
    "claim": {
      "certificateNumber": {
        "type": "string",
        "description": "certificate number"
      },
      "landType": {
        "type": "string",
        "description": "land type"
      },
      "totalArea": {
        "type": "string",
        "description": "total area"
      },
      "exclusiveArea": {
        "type": "string",
        "description": "exclusive area"
      },
      "sharingArea": {
        "type": "string",
        "description": "sharing area"
      },
      "useType": {
        "type": "string",
        "description": "use type"
      },
      "landNumber": {
        "type": "string",
        "description": "land number"
      },
      "holder": {
        "type": "string",
        "description": "holder"
      },
      "gettingPrice": {
        "type": "string",
        "description": "getting price"
      },
      "site": {
        "type": "string",
        "description": "site"
      }
    }
  },
  "required": [
    "landType",
    "totalArea"
  ],
  "supervisable": false,
  "url": "http://chinatrust.com",
  "weId": "did:weid:100:0xc204e875442db20b7316f726f0c1dccae7d39b3d"
}
```

#### 返回参数 

***1）出参表***

| 序号 | 输出参数     | 类型   | 可为空 | 备注                       |
| ---- | ------------ | ------ | ------ | -------------------------- |
| 1    | errorCode    | int    | 否     | 返回码，0：成功 其它：失败 |
| 2    | errorMessage | String | 否     | 描述                       |
| 3    | result       | Object | 是     | 信息对象                   |

***2）出参示例***

```
{
  "result": {
    "cptId": 2000000,
    "cptVersion": 1
  },
  "errorCode": 0,
  "errorMessage": "success",
  "transactionInfo": {
    "blockNumber": 778,
    "transactionHash": "0x103ede138aaaa25d86556bf4b82ffe701a69c2bade1d6b103bccae555a5f1f1d",
    "transactionIndex": 0
  }
}
```

### 4.2 查询CPT信息

根据cptId查询CPT信息

#### 传输协议规范

- 网络传输协议：使用HTTP协议
- 请求地址：**/step2/queryCpt?cptId**
- 请求方式：POST
- 返回格式：JSON

#### 请求参数

***1）入参表***

| 序号 | 输入参数 | 类型    | 可为空 | 备注    |
| ---- | -------- | ------- | ------ | ------- |
| 1    | cptId    | Integer | 否     | CPT编号 |

***2）入参示例***

```
http://127.0.0.1:6101/chinatrust-demo/step2/queryCpt?cptId=2000000
```

#### 返回参数 

***1）出参表***

| 序号 | 输出参数     | 类型   | 可为空 | 备注                       |
| ---- | ------------ | ------ | ------ | -------------------------- |
| 1    | errorCode    | int    | 否     | 返回码，0：成功 其它：失败 |
| 2    | errorMessage | String | 否     | 描述                       |
| 3    | result       | Object | 是     | 信息对象                   |

***2）出参示例***

```
{
  "result": {
    "cptBaseInfo": {
      "cptId": 2000000,
      "cptVersion": 1
    },
    "cptJsonSchema": {
      "cptDescription": "unit land certificate",
      "credentialType": {
        "cptTradeToken": "0x45d617c5157e9a07ac570d18692d362c3e50c852"
      },
      "cptType": "original",
      "$schema": "http://json-schema.org/draft-04/schema#",
      "cptManagment": "0x57bc072ab8af771c0978f94bd3c9c76ad0cc6eff",
      "supervisable": "false",
      "type": "object",
      "cptName": "landcertificate",
      "properties": {
        "claim": {
          "certificateNumber": {
            "description": "certificate number",
            "type": "string"
          },
          "exclusiveArea": {
            "description": "exclusive area",
            "type": "string"
          },
          "gettingPrice": {
            "description": "getting price",
            "type": "string"
          },
          "holder": {
            "description": "holder",
            "type": "string"
          },
          "landNumber": {
            "description": "land number",
            "type": "string"
          },
          "landType": {
            "description": "land type",
            "type": "string"
          },
          "sharingArea": {
            "description": "sharing area",
            "type": "string"
          },
          "site": {
            "description": "site",
            "type": "string"
          },
          "totalArea": {
            "description": "total area",
            "type": "string"
          },
          "useType": {
            "description": "use type",
            "type": "string"
          }
        },
        "expirationDate": {
          "description": "expiration date",
          "type": "string"
        },
        "id": {
          "description": "credential id",
          "type": "string"
        },
        "issuanceDate": {
          "description": "issuance date",
          "type": "string"
        },
        "issuer": {
          "WeID": {
            "description": "the WeID of credential recipient",
            "type": "string"
          },
          "description": "the information of credential recipient",
          "name": {
            "description": "the name of credential recipient",
            "type": "string"
          },
          "type": "object"
        },
        "recipient": {
          "WeID": {
            "description": "the WeID of credential recipient",
            "type": "string"
          },
          "description": "the information of credential recipient",
          "name": {
            "description": "the name of credential recipient",
            "type": "string"
          },
          "type": "object"
        }
      },
      "required": [
        "landType",
        "totalArea"
      ],
      "url": "http://chinatrust.com"
    },
    "metaData": {
      "cptPublisher": "did:weid:100:0xc204e875442db20b7316f726f0c1dccae7d39b3d",
      "cptSignature": "ABnVA5FTH2AqfAOzL41TgTf2bra4Enm7idz+BxsjhXMSbfCwvK4VWuUYIwcVKnyKdRdcMAj1pYIC1TLXy96Ax+E=",
      "created": 1637221901,
      "updated": 0
    },
    "cptId": 2000000,
    "cptSignature": "ABnVA5FTH2AqfAOzL41TgTf2bra4Enm7idz+BxsjhXMSbfCwvK4VWuUYIwcVKnyKdRdcMAj1pYIC1TLXy96Ax+E=",
    "created": 1637221901,
    "updated": 0,
    "cptPublisher": "did:weid:100:0xc204e875442db20b7316f726f0c1dccae7d39b3d",
    "cptVersion": 1
  },
  "errorCode": 0,
  "errorMessage": "success",
  "transactionInfo": null
}
```

### 4.3 更新CPT信息

根据cptId更新CPT信息。

#### 传输协议规范

- 网络传输协议：使用HTTP协议
- 请求地址：**/step3/updateCpt**
- 请求方式：POST
- 返回格式：JSON

#### 请求参数

***1）入参表***

| 序号 | 输入参数       | 类型                | 可为空 | 备注                                      |
| ---- | -------------- | ------------------- | ------ | ----------------------------------------- |
| 1    | privateKey     | String              | 否     | 私钥                                      |
| 2    | weId           | String              | 否     | 发布者weId                                |
| 3    | cptId          | Integer             | 否     | 指定的CPT ID                              |
| 4    | cptName        | String              | 否     | CPT名字                                   |
| 5    | cptDescription | String              | 否     | CPT描述                                   |
| 6    | cptManagment   | String              | 否     | 权限管理合约地址                          |
| 7    | credentialType | Map<String, Object> | 否     | 文档类型（可信文档/可流转文档）           |
| 8    | url            | String              | 否     | 查看或申请基于该CPT签发的credential的网址 |
| 9    | supervisable   | String              | 否     | 是否支持监管（true/false）                |
| 10   | required       | String[]            | 否     | 必要属性                                  |
| 11   | properties     | Map<String, Object> | 否     | 具体业务属性字段                          |

***2）入参示例***

```
http://127.0.0.1:6101/chinatrust-demo/step3/updateCpt
```

```json
{
  "cptDescription": "unit land certificate",
  "cptId": 2000000,
  "cptManagment": "0x57bc072ab8af771c0978f94bd3c9c76ad0cc6eff",
  "cptName": "landcertificate",
  "credentialType": {
    "cptTradeToken": "0x45d617c5157e9a07ac570d18692d362c3e50c852"
  },
  "privateKey": "7537941240201473300322488397004461834560927189780643296560422282794341761545",
  "properties": {
    "claim": {
      "certificateNumber": {
        "type": "string",
        "description": "certificate number"
      },
      "landType": {
        "type": "string",
        "description": "land type"
      },
      "totalArea": {
        "type": "string",
        "description": "total area"
      },
      "exclusiveArea": {
        "type": "string",
        "description": "exclusive area"
      },
      "sharingArea": {
        "type": "string",
        "description": "sharing area"
      },
      "useType": {
        "type": "string",
        "description": "use type"
      },
      "landNumber": {
        "type": "string",
        "description": "land number"
      },
      "holder": {
        "type": "string",
        "description": "holder"
      },
      "gettingPrice": {
        "type": "string",
        "description": "getting price"
      },
      "site": {
        "type": "string",
        "description": "site"
      }
    }
  },
  "required": [
    "landType",
    "totalArea"
  ],
  "supervisable": false,
  "url": "http://chinatrust.com",
  "weId": "did:weid:100:0xc204e875442db20b7316f726f0c1dccae7d39b3d"
}
```

#### 返回参数 

***1）出参表***

| 序号 | 输出参数     | 类型   | 可为空 | 备注                       |
| ---- | ------------ | ------ | ------ | -------------------------- |
| 1    | errorCode    | int    | 否     | 返回码，0：成功 其它：失败 |
| 2    | errorMessage | String | 否     | 描述                       |
| 3    | result       | Object | 是     | 信息对象                   |

***2）出参示例***

```
{
  "result": {
    "cptId": 2000000,
    "cptVersion": 2
  },
  "errorCode": 0,
  "errorMessage": "success",
  "transactionInfo": {
    "blockNumber": 779,
    "transactionHash": "0x3c0568046f48fe07622cc301c1c54c8c472cfdbf7f798b73b0e422a25a66c8f2",
    "transactionIndex": 0
  }
}
```

### 4.4 查询CPT数量

#### 传输协议规范

- 网络传输协议：使用HTTP协议
- 请求地址：**/step4/getCptCount**
- 请求方式：GET
- 返回格式：JSON

#### 请求参数

***1）入参表***

***2）入参示例***

```
http://127.0.0.1:6101/chinatrust-demo/step4/getCptCount
```

#### 返回参数 

***1）出参表***

| 序号 | 输出参数     | 类型   | 可为空 | 备注                       |
| ---- | ------------ | ------ | ------ | -------------------------- |
| 1    | errorCode    | int    | 否     | 返回码，0：成功 其它：失败 |
| 2    | errorMessage | String | 否     | 描述                       |
| 3    | result       | Object | 是     | 信息对象                   |

***2）出参示例***

```
{
  "result": 2,
  "errorCode": 0,
  "errorMessage": "success",
  "transactionInfo": null
}
```

### 4.5 查询CPT编号列表

#### 传输协议规范

- 网络传输协议：使用HTTP协议
- 请求地址：**/step5/getCptIdList**
- 请求方式：GET
- 返回格式：JSON

#### 请求参数

***1）入参表***

| 序号 | 输入参数 | 类型 | 可为空 | 备注     |
| ---- | -------- | ---- | ------ | -------- |
| 1    | num      | int  | 否     | 条数     |
| 2    | startPos | int  | 否     | 起始位置 |

***2）入参示例***

```
http://127.0.0.1:6101/chinatrust-demo/step5/getCptIdList?num=10&startPos=0
```

#### 返回参数 

***1）出参表***

| 序号 | 输出参数     | 类型   | 可为空 | 备注                       |
| ---- | ------------ | ------ | ------ | -------------------------- |
| 1    | errorCode    | int    | 否     | 返回码，0：成功 其它：失败 |
| 2    | errorMessage | String | 否     | 描述                       |
| 3    | result       | Object | 是     | 信息对象                   |

***2）出参示例***

```
{
  "result": [
    2000000,
    2000001
  ],
  "errorCode": 0,
  "errorMessage": "success",
  "transactionInfo": null
}
```

## 5 电子凭证接口

### 5.1 创建电子凭证

#### 传输协议规范

- 网络传输协议：使用HTTP协议
- 请求地址：**/step1/createCredential**
- 请求方式：POST
- 返回格式：JSON

#### 请求参数

***1）入参表***

| 序号 | 输入参数      | 类型                | 可为空 | 备注         |
| ---- | ------------- | ------------------- | ------ | ------------ |
| 1    | privateKey    | String              | 否     | 发行方私钥   |
| 2    | cptId         | Integer             | 否     | 指定的CPT ID |
| 3    | issuer        | String              | 否     | 发行方weId   |
| 4    | issuerName    | String              | 否     | 发行方名称   |
| 5    | recipient     | String              | 否     | 接收方weId   |
| 6    | recipientName | String              | 否     | 接收方名称   |
| 7    | claim         | Map<String, Object> | 否     | claim数据    |

***2）入参示例***

```
http://127.0.0.1:6101/chinatrust-demo/step1/createCredential
```

```json
{
  "claim": {
    "certificateNumber": "123456",
    "landType": "commerce",
    "totalArea": "15568.26m^2",
    "exclusiveArea": "14000m^2",
    "sharingArea": "1568.26m^2",
    "useType": "selling",
    "landNumber": "56-20-12",
    "holder": "China Evergrande",
    "gettingPrice": "500,000,000RMB",
    "site": "ShaHe XiLu 001"
  },
  "cptId": 2000000,
  "issuer": "did:weid:100:0xc204e875442db20b7316f726f0c1dccae7d39b3d",
  "issuerName": "China Land Managment",
  "privateKey": "7537941240201473300322488397004461834560927189780643296560422282794341761545",
  "recipient": "did:weid:100:0xc204e875442db20b7316f726f0c1dccae7d39b3d",
  "recipientName": "China Evergrande"
}
```

#### 返回参数 

***1）出参表***

| 序号 | 输出参数     | 类型   | 可为空 | 备注                       |
| ---- | ------------ | ------ | ------ | -------------------------- |
| 1    | errorCode    | int    | 否     | 返回码，0：成功 其它：失败 |
| 2    | errorMessage | String | 否     | 描述                       |
| 3    | result       | Object | 是     | 信息对象                   |

***2）出参示例***

```
{
  "result": {
    "type": "object",
    "cptBaseInfo": {
      "cptId": 2000000,
      "cptVersion": 2
    },
    "ctCptMapArgs": {
      "weIdAuthentication": null,
      "cptType": "ORIGINAL",
      "cptName": "landcertificate",
      "cptDescription": "unit land certificate",
      "cptManagment": "0x57bc072ab8af771c0978f94bd3c9c76ad0cc6eff",
      "credentialType": {
        "cptTradeToken": "0x45d617c5157e9a07ac570d18692d362c3e50c852"
      },
      "url": "http://chinatrust.com",
      "supervisable": "false",
      "properties": null,
      "required": [
        "landType",
        "totalArea"
      ]
    },
    "metaData": {
      "cptPublisher": "did:weid:100:0xc204e875442db20b7316f726f0c1dccae7d39b3d",
      "cptSignature": "ABnVA5FTH2AqfAOzL41TgTf2bra4Enm7idz+BxsjhXMSbfCwvK4VWuUYIwcVKnyKdRdcMAj1pYIC1TLXy96Ax+E=",
      "created": 1637221901,
      "updated": 0
    },
    "properties": {
      "recipient": {
        "WeID": "did:weid:100:0xc204e875442db20b7316f726f0c1dccae7d39b3d",
        "name": "China Evergrande"
      },
      "claim": {
        "exclusiveArea": "14000m^2",
        "site": "ShaHe XiLu 001",
        "gettingPrice": "500,000,000RMB",
        "landNumber": "56-20-12",
        "certificateNumber": "123456",
        "holder": "China Evergrande",
        "landType": "commerce",
        "useType": "selling",
        "sharingArea": "1568.26m^2",
        "totalArea": "15568.26m^2"
      },
      "id": "303d80dc-0b6d-4fca-bef9-ebfeec615bf8",
      "issuanceDate": 1637223133,
      "issuer": {
        "WeID": "did:weid:100:0xc204e875442db20b7316f726f0c1dccae7d39b3d",
        "name": "China Land Managment"
      },
      "expirationDate": 4790823133
    },
    "context": "https://github.com/WeBankFinTech/WeIdentity/blob/master/context/v1",
    "docType": [
      "VerifiableCredential",
      "original"
    ],
    "proof": {
      "creator": "did:weid:100:0xc204e875442db20b7316f726f0c1dccae7d39b3dkey-0",
      "salt": {
        "exclusiveArea": "cOOlP",
        "site": "2lhk9",
        "gettingPrice": "prAjx",
        "landNumber": "pLQ4w",
        "certificateNumber": "m4frS",
        "holder": "rp1Vz",
        "landType": "zkXbK",
        "useType": "YfOL6",
        "sharingArea": "ur5vH",
        "totalArea": "isnEE"
      },
      "created": 1637223133,
      "type": "Secp256k1",
      "signatureValue": "b2XO3Orb5AwlN/ZLTpKUwCbQcXFtbQcmc26n8xJ+oFx2B68lxbd3c39JDnXZl1yaNeO+G6dSWMWIQnwuspbQZAE="
    },
    "claim": {
      "exclusiveArea": "14000m^2",
      "site": "ShaHe XiLu 001",
      "gettingPrice": "500,000,000RMB",
      "landNumber": "56-20-12",
      "certificateNumber": "123456",
      "holder": "China Evergrande",
      "landType": "commerce",
      "useType": "selling",
      "sharingArea": "1568.26m^2",
      "totalArea": "15568.26m^2"
    },
    "salt": {
      "exclusiveArea": "cOOlP",
      "site": "2lhk9",
      "gettingPrice": "prAjx",
      "landNumber": "pLQ4w",
      "certificateNumber": "m4frS",
      "holder": "rp1Vz",
      "landType": "zkXbK",
      "useType": "YfOL6",
      "sharingArea": "ur5vH",
      "totalArea": "isnEE"
    },
    "signatureTargetData": "{\"claim\":{\"certificateNumber\":\"0xb46188f88fffdea5c23670c1ce7659ec10cda491769a48bc042062b3db842d3f\",\"exclusiveArea\":\"0xed5f7c39f828492cd829cda2cfb03a744479f1201affed3c5001ff5663f7a6bc\",\"gettingPrice\":\"0xfe9edb934d330902d2b64af782f0876112cb3a44453650a2ed732c67fd2b82f9\",\"holder\":\"0x2b3cd7ef9262dcfce2b52f4ff520c5e10786b15e635422fe1a760f292e9526f5\",\"landNumber\":\"0x4731381184521972413208d04e247344e6d5520ee1ebe0770eb2552c4f65d248\",\"landType\":\"0xc8e2cdd10d622b4a45e7067c43b817d9293d6ef98baea9692f412a40d7128641\",\"sharingArea\":\"0xaae15458e7198ce67e7d4fcf045a2239a9e5f9720de1f342f7f01c78c0e6a6d5\",\"site\":\"0x016c850f5e9f1ef7ddcc925d426f84b0b4774733a356a93bd3cc024510caefc9\",\"totalArea\":\"0x4414a22d0e01de0ec994d4d05c5a95ce7e9b66e8fe5ecb305eaf3730ce983d50\",\"useType\":\"0x6253b1a3a66895131a3e7c22625522faaa26bbde2f811edb365aa713463a8ef4\"},\"context\":\"https://github.com/WeBankFinTech/WeIdentity/blob/master/context/v1\",\"cptId\":2000000,\"expirationDate\":4790823133,\"id\":\"303d80dc-0b6d-4fca-bef9-ebfeec615bf8\",\"issuanceDate\":1637223133,\"issuer\":\"did:weid:100:0xc204e875442db20b7316f726f0c1dccae7d39b3d\",\"type\":[\"VerifiableCredential\",\"original\"]}",
    "signatureTargetHash": "[B@3d76cf83"
  },
  "errorCode": 0,
  "errorMessage": "success",
  "transactionInfo": null
}
```

### 5.2 获取电子凭证hash

根据cptId查询CPT信息

#### 传输协议规范

- 网络传输协议：使用HTTP协议
- 请求地址：**/step2/getCredentialPojoHash**
- 请求方式：POST
- 返回格式：JSON

#### 请求参数

***1）入参表***

| 序号 | 输入参数   | 类型                | 可为空 | 备注     |
| ---- | ---------- | ------------------- | ------ | -------- |
| 1    | credential | Map<String, Object> | 否     | 电子凭证 |

***2）入参示例***

```
http://127.0.0.1:6101/chinatrust-demo/step2/getCredentialPojoHash
```

```json
{
  "credential": {
    "type": "object",
    "cptBaseInfo": {
      "cptId": 2000000,
      "cptVersion": 2
    },
    "ctCptMapArgs": {
      "weIdAuthentication": null,
      "cptType": "ORIGINAL",
      "cptName": "landcertificate",
      "cptDescription": "unit land certificate",
      "cptManagment": "0x57bc072ab8af771c0978f94bd3c9c76ad0cc6eff",
      "credentialType": {
        "cptTradeToken": "0x45d617c5157e9a07ac570d18692d362c3e50c852"
      },
      "url": "http://chinatrust.com",
      "supervisable": "false",
      "properties": null,
      "required": [
        "landType",
        "totalArea"
      ]
    },
    "metaData": {
      "cptPublisher": "did:weid:100:0xc204e875442db20b7316f726f0c1dccae7d39b3d",
      "cptSignature": "ABnVA5FTH2AqfAOzL41TgTf2bra4Enm7idz+BxsjhXMSbfCwvK4VWuUYIwcVKnyKdRdcMAj1pYIC1TLXy96Ax+E=",
      "created": 1637221901,
      "updated": 0
    },
    "properties": {
      "recipient": {
        "WeID": "did:weid:100:0xc204e875442db20b7316f726f0c1dccae7d39b3d",
        "name": "China Evergrande"
      },
      "claim": {
        "exclusiveArea": "14000m^2",
        "site": "ShaHe XiLu 001",
        "gettingPrice": "500,000,000RMB",
        "landNumber": "56-20-12",
        "certificateNumber": "123456",
        "holder": "China Evergrande",
        "landType": "commerce",
        "useType": "selling",
        "sharingArea": "1568.26m^2",
        "totalArea": "15568.26m^2"
      },
      "id": "303d80dc-0b6d-4fca-bef9-ebfeec615bf8",
      "issuanceDate": 1637223133,
      "issuer": {
        "WeID": "did:weid:100:0xc204e875442db20b7316f726f0c1dccae7d39b3d",
        "name": "China Land Managment"
      },
      "expirationDate": 4790823133
    },
    "context": "https://github.com/WeBankFinTech/WeIdentity/blob/master/context/v1",
    "docType": [
      "VerifiableCredential",
      "original"
    ],
    "proof": {
      "creator": "did:weid:100:0xc204e875442db20b7316f726f0c1dccae7d39b3dkey-0",
      "salt": {
        "exclusiveArea": "cOOlP",
        "site": "2lhk9",
        "gettingPrice": "prAjx",
        "landNumber": "pLQ4w",
        "certificateNumber": "m4frS",
        "holder": "rp1Vz",
        "landType": "zkXbK",
        "useType": "YfOL6",
        "sharingArea": "ur5vH",
        "totalArea": "isnEE"
      },
      "created": 1637223133,
      "type": "Secp256k1",
      "signatureValue": "b2XO3Orb5AwlN/ZLTpKUwCbQcXFtbQcmc26n8xJ+oFx2B68lxbd3c39JDnXZl1yaNeO+G6dSWMWIQnwuspbQZAE="
    },
    "claim": {
      "exclusiveArea": "14000m^2",
      "site": "ShaHe XiLu 001",
      "gettingPrice": "500,000,000RMB",
      "landNumber": "56-20-12",
      "certificateNumber": "123456",
      "holder": "China Evergrande",
      "landType": "commerce",
      "useType": "selling",
      "sharingArea": "1568.26m^2",
      "totalArea": "15568.26m^2"
    },
    "salt": {
      "exclusiveArea": "cOOlP",
      "site": "2lhk9",
      "gettingPrice": "prAjx",
      "landNumber": "pLQ4w",
      "certificateNumber": "m4frS",
      "holder": "rp1Vz",
      "landType": "zkXbK",
      "useType": "YfOL6",
      "sharingArea": "ur5vH",
      "totalArea": "isnEE"
    },
    "signatureTargetData": "{\"claim\":{\"certificateNumber\":\"0xb46188f88fffdea5c23670c1ce7659ec10cda491769a48bc042062b3db842d3f\",\"exclusiveArea\":\"0xed5f7c39f828492cd829cda2cfb03a744479f1201affed3c5001ff5663f7a6bc\",\"gettingPrice\":\"0xfe9edb934d330902d2b64af782f0876112cb3a44453650a2ed732c67fd2b82f9\",\"holder\":\"0x2b3cd7ef9262dcfce2b52f4ff520c5e10786b15e635422fe1a760f292e9526f5\",\"landNumber\":\"0x4731381184521972413208d04e247344e6d5520ee1ebe0770eb2552c4f65d248\",\"landType\":\"0xc8e2cdd10d622b4a45e7067c43b817d9293d6ef98baea9692f412a40d7128641\",\"sharingArea\":\"0xaae15458e7198ce67e7d4fcf045a2239a9e5f9720de1f342f7f01c78c0e6a6d5\",\"site\":\"0x016c850f5e9f1ef7ddcc925d426f84b0b4774733a356a93bd3cc024510caefc9\",\"totalArea\":\"0x4414a22d0e01de0ec994d4d05c5a95ce7e9b66e8fe5ecb305eaf3730ce983d50\",\"useType\":\"0x6253b1a3a66895131a3e7c22625522faaa26bbde2f811edb365aa713463a8ef4\"},\"context\":\"https://github.com/WeBankFinTech/WeIdentity/blob/master/context/v1\",\"cptId\":2000000,\"expirationDate\":4790823133,\"id\":\"303d80dc-0b6d-4fca-bef9-ebfeec615bf8\",\"issuanceDate\":1637223133,\"issuer\":\"did:weid:100:0xc204e875442db20b7316f726f0c1dccae7d39b3d\",\"type\":[\"VerifiableCredential\",\"original\"]}",
    "signatureTargetHash": "[B@3d76cf83"
  }
}
```

#### 返回参数 

***1）出参表***

| 序号 | 输出参数     | 类型   | 可为空 | 备注                       |
| ---- | ------------ | ------ | ------ | -------------------------- |
| 1    | errorCode    | int    | 否     | 返回码，0：成功 其它：失败 |
| 2    | errorMessage | String | 否     | 描述                       |
| 3    | result       | Object | 是     | 信息对象                   |

***2）出参示例***

```
{
  "result": "0xa24e29dfacb3fe4e913e70961221a6ece5bb1e64fc4a257a9758ed8618cf39fc",
  "errorCode": 0,
  "errorMessage": "success",
  "transactionInfo": null
}
```

### 5.3 验证电子凭证

#### 传输协议规范

- 网络传输协议：使用HTTP协议
- 请求地址：**/step3/verify**
- 请求方式：POST
- 返回格式：JSON

#### 请求参数

***1）入参表***

| 序号 | 输入参数   | 类型                | 可为空 | 备注       |
| ---- | ---------- | ------------------- | ------ | ---------- |
| 1    | credential | Map<String, Object> | 否     | 电子凭证   |
| 2    | issuerWeId | String              | 否     | 发布者weId |

***2）入参示例***

```
http://127.0.0.1:6101/chinatrust-demo/step3/verify
```

```json
{
  "credential": {
    "type": "object",
    "cptBaseInfo": {
      "cptId": 2000000,
      "cptVersion": 2
    },
    "ctCptMapArgs": {
      "weIdAuthentication": null,
      "cptType": "ORIGINAL",
      "cptName": "landcertificate",
      "cptDescription": "unit land certificate",
      "cptManagment": "0x57bc072ab8af771c0978f94bd3c9c76ad0cc6eff",
      "credentialType": {
        "cptTradeToken": "0x45d617c5157e9a07ac570d18692d362c3e50c852"
      },
      "url": "http://chinatrust.com",
      "supervisable": "false",
      "properties": null,
      "required": [
        "landType",
        "totalArea"
      ]
    },
    "metaData": {
      "cptPublisher": "did:weid:100:0xc204e875442db20b7316f726f0c1dccae7d39b3d",
      "cptSignature": "ABnVA5FTH2AqfAOzL41TgTf2bra4Enm7idz+BxsjhXMSbfCwvK4VWuUYIwcVKnyKdRdcMAj1pYIC1TLXy96Ax+E=",
      "created": 1637221901,
      "updated": 0
    },
    "properties": {
      "recipient": {
        "WeID": "did:weid:100:0xc204e875442db20b7316f726f0c1dccae7d39b3d",
        "name": "China Evergrande"
      },
      "claim": {
        "exclusiveArea": "14000m^2",
        "site": "ShaHe XiLu 001",
        "gettingPrice": "500,000,000RMB",
        "landNumber": "56-20-12",
        "certificateNumber": "123456",
        "holder": "China Evergrande",
        "landType": "commerce",
        "useType": "selling",
        "sharingArea": "1568.26m^2",
        "totalArea": "15568.26m^2"
      },
      "id": "303d80dc-0b6d-4fca-bef9-ebfeec615bf8",
      "issuanceDate": 1637223133,
      "issuer": {
        "WeID": "did:weid:100:0xc204e875442db20b7316f726f0c1dccae7d39b3d",
        "name": "China Land Managment"
      },
      "expirationDate": 4790823133
    },
    "context": "https://github.com/WeBankFinTech/WeIdentity/blob/master/context/v1",
    "docType": [
      "VerifiableCredential",
      "original"
    ],
    "proof": {
      "creator": "did:weid:100:0xc204e875442db20b7316f726f0c1dccae7d39b3dkey-0",
      "salt": {
        "exclusiveArea": "cOOlP",
        "site": "2lhk9",
        "gettingPrice": "prAjx",
        "landNumber": "pLQ4w",
        "certificateNumber": "m4frS",
        "holder": "rp1Vz",
        "landType": "zkXbK",
        "useType": "YfOL6",
        "sharingArea": "ur5vH",
        "totalArea": "isnEE"
      },
      "created": 1637223133,
      "type": "Secp256k1",
      "signatureValue": "b2XO3Orb5AwlN/ZLTpKUwCbQcXFtbQcmc26n8xJ+oFx2B68lxbd3c39JDnXZl1yaNeO+G6dSWMWIQnwuspbQZAE="
    },
    "claim": {
      "exclusiveArea": "14000m^2",
      "site": "ShaHe XiLu 001",
      "gettingPrice": "500,000,000RMB",
      "landNumber": "56-20-12",
      "certificateNumber": "123456",
      "holder": "China Evergrande",
      "landType": "commerce",
      "useType": "selling",
      "sharingArea": "1568.26m^2",
      "totalArea": "15568.26m^2"
    },
    "salt": {
      "exclusiveArea": "cOOlP",
      "site": "2lhk9",
      "gettingPrice": "prAjx",
      "landNumber": "pLQ4w",
      "certificateNumber": "m4frS",
      "holder": "rp1Vz",
      "landType": "zkXbK",
      "useType": "YfOL6",
      "sharingArea": "ur5vH",
      "totalArea": "isnEE"
    },
    "signatureTargetData": "{\"claim\":{\"certificateNumber\":\"0xb46188f88fffdea5c23670c1ce7659ec10cda491769a48bc042062b3db842d3f\",\"exclusiveArea\":\"0xed5f7c39f828492cd829cda2cfb03a744479f1201affed3c5001ff5663f7a6bc\",\"gettingPrice\":\"0xfe9edb934d330902d2b64af782f0876112cb3a44453650a2ed732c67fd2b82f9\",\"holder\":\"0x2b3cd7ef9262dcfce2b52f4ff520c5e10786b15e635422fe1a760f292e9526f5\",\"landNumber\":\"0x4731381184521972413208d04e247344e6d5520ee1ebe0770eb2552c4f65d248\",\"landType\":\"0xc8e2cdd10d622b4a45e7067c43b817d9293d6ef98baea9692f412a40d7128641\",\"sharingArea\":\"0xaae15458e7198ce67e7d4fcf045a2239a9e5f9720de1f342f7f01c78c0e6a6d5\",\"site\":\"0x016c850f5e9f1ef7ddcc925d426f84b0b4774733a356a93bd3cc024510caefc9\",\"totalArea\":\"0x4414a22d0e01de0ec994d4d05c5a95ce7e9b66e8fe5ecb305eaf3730ce983d50\",\"useType\":\"0x6253b1a3a66895131a3e7c22625522faaa26bbde2f811edb365aa713463a8ef4\"},\"context\":\"https://github.com/WeBankFinTech/WeIdentity/blob/master/context/v1\",\"cptId\":2000000,\"expirationDate\":4790823133,\"id\":\"303d80dc-0b6d-4fca-bef9-ebfeec615bf8\",\"issuanceDate\":1637223133,\"issuer\":\"did:weid:100:0xc204e875442db20b7316f726f0c1dccae7d39b3d\",\"type\":[\"VerifiableCredential\",\"original\"]}",
    "signatureTargetHash": "[B@3d76cf83"
  },
  "issuerWeId": "did:weid:100:0xc204e875442db20b7316f726f0c1dccae7d39b3d"
}
```

#### 返回参数 

***1）出参表***

| 序号 | 输出参数     | 类型   | 可为空 | 备注                       |
| ---- | ------------ | ------ | ------ | -------------------------- |
| 1    | errorCode    | int    | 否     | 返回码，0：成功 其它：失败 |
| 2    | errorMessage | String | 否     | 描述                       |
| 3    | result       | Object | 是     | 信息对象                   |

***2）出参示例***

```
{
  "result": true,
  "errorCode": 0,
  "errorMessage": "success",
  "transactionInfo": null
}
```

### 5.4 创建选择性披露电子凭证

#### 传输协议规范

- 网络传输协议：使用HTTP协议
- 请求地址：**/step4/createSelectiveCredential**
- 请求方式：POST
- 返回格式：JSON

#### 请求参数

***1）入参表***

| 序号 | 输入参数            | 类型                | 可为空 | 备注     |
| ---- | ------------------- | ------------------- | ------ | -------- |
| 1    | credential          | Map<String, Object> | 否     | 电子凭证 |
| 2    | fieldsToBeDisclosed | Map<String, Object> | 否     | 披露配置 |

***2）入参示例***

```
http://127.0.0.1:6101/chinatrust-demo/step4/createSelectiveCredential
```

```json
{
  "credential": {
    "type": "object",
    "cptBaseInfo": {
      "cptId": 2000000,
      "cptVersion": 2
    },
    "ctCptMapArgs": {
      "weIdAuthentication": null,
      "cptType": "ORIGINAL",
      "cptName": "landcertificate",
      "cptDescription": "unit land certificate",
      "cptManagment": "0x57bc072ab8af771c0978f94bd3c9c76ad0cc6eff",
      "credentialType": {
        "cptTradeToken": "0x45d617c5157e9a07ac570d18692d362c3e50c852"
      },
      "url": "http://chinatrust.com",
      "supervisable": "false",
      "properties": null,
      "required": [
        "landType",
        "totalArea"
      ]
    },
    "metaData": {
      "cptPublisher": "did:weid:100:0xc204e875442db20b7316f726f0c1dccae7d39b3d",
      "cptSignature": "ABnVA5FTH2AqfAOzL41TgTf2bra4Enm7idz+BxsjhXMSbfCwvK4VWuUYIwcVKnyKdRdcMAj1pYIC1TLXy96Ax+E=",
      "created": 1637221901,
      "updated": 0
    },
    "properties": {
      "recipient": {
        "WeID": "did:weid:100:0xc204e875442db20b7316f726f0c1dccae7d39b3d",
        "name": "China Evergrande"
      },
      "claim": {
        "exclusiveArea": "14000m^2",
        "site": "ShaHe XiLu 001",
        "gettingPrice": "500,000,000RMB",
        "landNumber": "56-20-12",
        "certificateNumber": "123456",
        "holder": "China Evergrande",
        "landType": "commerce",
        "useType": "selling",
        "sharingArea": "1568.26m^2",
        "totalArea": "15568.26m^2"
      },
      "id": "303d80dc-0b6d-4fca-bef9-ebfeec615bf8",
      "issuanceDate": 1637223133,
      "issuer": {
        "WeID": "did:weid:100:0xc204e875442db20b7316f726f0c1dccae7d39b3d",
        "name": "China Land Managment"
      },
      "expirationDate": 4790823133
    },
    "context": "https://github.com/WeBankFinTech/WeIdentity/blob/master/context/v1",
    "docType": [
      "VerifiableCredential",
      "original"
    ],
    "proof": {
      "creator": "did:weid:100:0xc204e875442db20b7316f726f0c1dccae7d39b3dkey-0",
      "salt": {
        "exclusiveArea": "cOOlP",
        "site": "2lhk9",
        "gettingPrice": "prAjx",
        "landNumber": "pLQ4w",
        "certificateNumber": "m4frS",
        "holder": "rp1Vz",
        "landType": "zkXbK",
        "useType": "YfOL6",
        "sharingArea": "ur5vH",
        "totalArea": "isnEE"
      },
      "created": 1637223133,
      "type": "Secp256k1",
      "signatureValue": "b2XO3Orb5AwlN/ZLTpKUwCbQcXFtbQcmc26n8xJ+oFx2B68lxbd3c39JDnXZl1yaNeO+G6dSWMWIQnwuspbQZAE="
    },
    "claim": {
      "exclusiveArea": "14000m^2",
      "site": "ShaHe XiLu 001",
      "gettingPrice": "500,000,000RMB",
      "landNumber": "56-20-12",
      "certificateNumber": "123456",
      "holder": "China Evergrande",
      "landType": "commerce",
      "useType": "selling",
      "sharingArea": "1568.26m^2",
      "totalArea": "15568.26m^2"
    },
    "salt": {
      "exclusiveArea": "cOOlP",
      "site": "2lhk9",
      "gettingPrice": "prAjx",
      "landNumber": "pLQ4w",
      "certificateNumber": "m4frS",
      "holder": "rp1Vz",
      "landType": "zkXbK",
      "useType": "YfOL6",
      "sharingArea": "ur5vH",
      "totalArea": "isnEE"
    },
    "signatureTargetData": "{\"claim\":{\"certificateNumber\":\"0xb46188f88fffdea5c23670c1ce7659ec10cda491769a48bc042062b3db842d3f\",\"exclusiveArea\":\"0xed5f7c39f828492cd829cda2cfb03a744479f1201affed3c5001ff5663f7a6bc\",\"gettingPrice\":\"0xfe9edb934d330902d2b64af782f0876112cb3a44453650a2ed732c67fd2b82f9\",\"holder\":\"0x2b3cd7ef9262dcfce2b52f4ff520c5e10786b15e635422fe1a760f292e9526f5\",\"landNumber\":\"0x4731381184521972413208d04e247344e6d5520ee1ebe0770eb2552c4f65d248\",\"landType\":\"0xc8e2cdd10d622b4a45e7067c43b817d9293d6ef98baea9692f412a40d7128641\",\"sharingArea\":\"0xaae15458e7198ce67e7d4fcf045a2239a9e5f9720de1f342f7f01c78c0e6a6d5\",\"site\":\"0x016c850f5e9f1ef7ddcc925d426f84b0b4774733a356a93bd3cc024510caefc9\",\"totalArea\":\"0x4414a22d0e01de0ec994d4d05c5a95ce7e9b66e8fe5ecb305eaf3730ce983d50\",\"useType\":\"0x6253b1a3a66895131a3e7c22625522faaa26bbde2f811edb365aa713463a8ef4\"},\"context\":\"https://github.com/WeBankFinTech/WeIdentity/blob/master/context/v1\",\"cptId\":2000000,\"expirationDate\":4790823133,\"id\":\"303d80dc-0b6d-4fca-bef9-ebfeec615bf8\",\"issuanceDate\":1637223133,\"issuer\":\"did:weid:100:0xc204e875442db20b7316f726f0c1dccae7d39b3d\",\"type\":[\"VerifiableCredential\",\"original\"]}",
    "signatureTargetHash": "[B@3d76cf83"
  },
  "fieldsToBeDisclosed": {
    "landNumber": 1,
    "gettingPrice": 1,
    "totalArea": 1
  }
}
```

#### 返回参数 

***1）出参表***

| 序号 | 输出参数     | 类型   | 可为空 | 备注                       |
| ---- | ------------ | ------ | ------ | -------------------------- |
| 1    | errorCode    | int    | 否     | 返回码，0：成功 其它：失败 |
| 2    | errorMessage | String | 否     | 描述                       |
| 3    | result       | Object | 是     | 信息对象                   |

***2）出参示例***

```
{
  "result": {
    "type": "object",
    "cptBaseInfo": {
      "cptId": 2000000,
      "cptVersion": 2
    },
    "ctCptMapArgs": {
      "weIdAuthentication": null,
      "cptType": "ORIGINAL",
      "cptName": "landcertificate",
      "cptDescription": "unit land certificate",
      "cptManagment": "0x57bc072ab8af771c0978f94bd3c9c76ad0cc6eff",
      "credentialType": {
        "cptTradeToken": "0x45d617c5157e9a07ac570d18692d362c3e50c852"
      },
      "url": "http://chinatrust.com",
      "supervisable": "false",
      "properties": null,
      "required": [
        "landType",
        "totalArea"
      ]
    },
    "metaData": {
      "cptPublisher": "did:weid:100:0xc204e875442db20b7316f726f0c1dccae7d39b3d",
      "cptSignature": "ABnVA5FTH2AqfAOzL41TgTf2bra4Enm7idz+BxsjhXMSbfCwvK4VWuUYIwcVKnyKdRdcMAj1pYIC1TLXy96Ax+E=",
      "created": 1637221901,
      "updated": 0
    },
    "properties": {
      "recipient": {
        "WeID": "did:weid:100:0xc204e875442db20b7316f726f0c1dccae7d39b3d",
        "name": "China Evergrande"
      },
      "claim": {
        "exclusiveArea": "0xed5f7c39f828492cd829cda2cfb03a744479f1201affed3c5001ff5663f7a6bc",
        "site": "0x016c850f5e9f1ef7ddcc925d426f84b0b4774733a356a93bd3cc024510caefc9",
        "gettingPrice": "500,000,000RMB",
        "landNumber": "56-20-12",
        "certificateNumber": "0xb46188f88fffdea5c23670c1ce7659ec10cda491769a48bc042062b3db842d3f",
        "holder": "0x2b3cd7ef9262dcfce2b52f4ff520c5e10786b15e635422fe1a760f292e9526f5",
        "landType": "0xc8e2cdd10d622b4a45e7067c43b817d9293d6ef98baea9692f412a40d7128641",
        "useType": "0x6253b1a3a66895131a3e7c22625522faaa26bbde2f811edb365aa713463a8ef4",
        "sharingArea": "0xaae15458e7198ce67e7d4fcf045a2239a9e5f9720de1f342f7f01c78c0e6a6d5",
        "totalArea": "15568.26m^2"
      },
      "id": "303d80dc-0b6d-4fca-bef9-ebfeec615bf8",
      "issuanceDate": 1637223133,
      "issuer": {
        "WeID": "did:weid:100:0xc204e875442db20b7316f726f0c1dccae7d39b3d",
        "name": "China Land Managment"
      },
      "expirationDate": 4790823133
    },
    "context": "https://github.com/WeBankFinTech/WeIdentity/blob/master/context/v1",
    "docType": [
      "VerifiableCredential",
      "original"
    ],
    "proof": {
      "created": 1637223133,
      "creator": "did:weid:100:0xc204e875442db20b7316f726f0c1dccae7d39b3dkey-0",
      "salt": {
        "certificateNumber": "m4frS",
        "exclusiveArea": "cOOlP",
        "gettingPrice": "prAjx",
        "holder": "rp1Vz",
        "landNumber": "pLQ4w",
        "landType": "zkXbK",
        "sharingArea": "ur5vH",
        "site": "2lhk9",
        "totalArea": "isnEE",
        "useType": "YfOL6"
      },
      "signatureValue": "b2XO3Orb5AwlN/ZLTpKUwCbQcXFtbQcmc26n8xJ+oFx2B68lxbd3c39JDnXZl1yaNeO+G6dSWMWIQnwuspbQZAE=",
      "type": "Secp256k1"
    },
    "claim": {
      "exclusiveArea": "0xed5f7c39f828492cd829cda2cfb03a744479f1201affed3c5001ff5663f7a6bc",
      "site": "0x016c850f5e9f1ef7ddcc925d426f84b0b4774733a356a93bd3cc024510caefc9",
      "gettingPrice": "500,000,000RMB",
      "landNumber": "56-20-12",
      "certificateNumber": "0xb46188f88fffdea5c23670c1ce7659ec10cda491769a48bc042062b3db842d3f",
      "holder": "0x2b3cd7ef9262dcfce2b52f4ff520c5e10786b15e635422fe1a760f292e9526f5",
      "landType": "0xc8e2cdd10d622b4a45e7067c43b817d9293d6ef98baea9692f412a40d7128641",
      "useType": "0x6253b1a3a66895131a3e7c22625522faaa26bbde2f811edb365aa713463a8ef4",
      "sharingArea": "0xaae15458e7198ce67e7d4fcf045a2239a9e5f9720de1f342f7f01c78c0e6a6d5",
      "totalArea": "15568.26m^2"
    },
    "salt": {
      "exclusiveArea": "cOOlP",
      "site": "2lhk9",
      "gettingPrice": "prAjx",
      "landNumber": "pLQ4w",
      "certificateNumber": "m4frS",
      "holder": "rp1Vz",
      "landType": "zkXbK",
      "useType": "YfOL6",
      "sharingArea": "ur5vH",
      "totalArea": "isnEE"
    },
    "signatureTargetData": "{\"claim\":{\"certificateNumber\":\"0x79e03538b1f0f119165de56926131ec7b2717b01bf89b89c1cb5465b62a471af\",\"exclusiveArea\":\"0xcf75f49166f35850dd91709fee426c32a634eb350847f6e176211c6f2de84c9f\",\"gettingPrice\":\"0xfe9edb934d330902d2b64af782f0876112cb3a44453650a2ed732c67fd2b82f9\",\"holder\":\"0xf2a0da1db79d951e6a1eba3e7f7a22c41e86a9f6db1511bde966d2df90e72993\",\"landNumber\":\"0x4731381184521972413208d04e247344e6d5520ee1ebe0770eb2552c4f65d248\",\"landType\":\"0xa2f4463dd8abd60c07e22c4a08b20c9250327611e17907c10de4c3e8ccb74c8c\",\"sharingArea\":\"0xd93a3fc84bdce9411372f4df7630fba12025e1d43f3bbb4926b5574d45ad750e\",\"site\":\"0x8d9d48404a45ab93769b27277082e65704b75f9dd16416a682d4308d95d71cb7\",\"totalArea\":\"0x4414a22d0e01de0ec994d4d05c5a95ce7e9b66e8fe5ecb305eaf3730ce983d50\",\"useType\":\"0x741e0885efdc100b3004f275fe6238149bb8102cd95ec19d9f48c430becbc972\"},\"context\":\"https://github.com/WeBankFinTech/WeIdentity/blob/master/context/v1\",\"cptId\":2000000,\"expirationDate\":4790823133,\"id\":\"303d80dc-0b6d-4fca-bef9-ebfeec615bf8\",\"issuanceDate\":1637223133,\"issuer\":\"did:weid:100:0xc204e875442db20b7316f726f0c1dccae7d39b3d\",\"type\":[\"VerifiableCredential\",\"original\"]}",
    "signatureTargetHash": "[B@2e6bb5ac"
  },
  "errorCode": 0,
  "errorMessage": "success",
  "transactionInfo": null
}
```