# 安装文档

## 1. 前提条件

| 环境 | 版本                                  |
| ---- | ------------------------------------- |
| Java | [JDK8或以上版本](./appendix.html#jdk) |

## 2. 拉取代码

执行命令：

```shell
git clone https://gitee.com/TransTrust/demo.git
```

进入目录：

```
cd demo && chmod +x *.sh
```

## 3. 编译代码

使用以下方式编译构建
方式一：如果服务器已安装Gradle，且版本为gradle-4.10或以上

```shell
gradle build -x test
```

方式二：如果服务器未安装Gradle，或者版本不是gradle-4.10或以上，使用gradlew编译

```shell
chmod +x ./gradlew && ./gradlew build -x test
```

构建完成后，会在根目录demo下生成已编译的代码目录dist。

## 4. 修改配置

### 4.1 复制证书

进入配置目录conf：

```shell
cd dist/conf
```

将节点所在目录`nodes/${ip}/sdk`下的所有文件拷贝到当前conf目录（包括ca.crt, sdk.crt, sdk.key, node.crt, node.key），供SDK与节点建立连接时使用。（若没有node.crt, node.key，可通过`cp`复制sdk.crt为node.crt, 复制sdk.key为node.key）

- 若使用的是**国密SSL模式**，则将`nodes/${ip}/sdk/gm`下的所有文件（包括gmca.crt, gmensdk.crt, gmensdk.key, gmsdk.crt, gmensdk.key）拷贝到当前conf目录（无需拷贝sdk目录下的sdk.crt等证书）。

### 4.2 修改配置

- 修改节点配置

```shell
vi weidentity.properties
```

```
# Blockchain node info.
nodes=127.0.0.1:20200
```

## 5. 服务启停

返回到dist目录执行：

```shell
启动：bash start.sh
停止：bash stop.sh
检查：bash status.sh
```

**备注**：服务进程起来后，需通过日志确认是否正常启动，出现以下内容表示正常；如果服务出现异常，确认修改配置后，重启提示服务进程在运行，则先执行stop.sh，再执行start.sh。

```
...
	Application() - main run success...
```

## 6. 访问

可以通过swagger查看调用接口：

```
http://{deployIP}:{deployPort}/TransTrust-demo/swagger-ui.html
示例：http://localhost:6101/TransTrust-demo/swagger-ui.html
```

## 7. 查看日志

在dist/logs目录查看：

```shell
服务日志：tail -f info.log
```