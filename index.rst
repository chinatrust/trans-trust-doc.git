##############################################################
TransTrust 技术文档
##############################################################

.. image:: ./images/logo.jpg

TransTrust 是一个文档数据格式化的标准和一套基于标准的技术参考实现。基于TransTrust能够跟踪数字发行文档的来源并验证其完整性，有助于不同数字生态系统之间交换的电子文件的互操作性。

.. container:: row 
   
   .. container:: card-holder
      
      .. container:: card rocket

         .. raw:: html

            <br>
            <h style="font-size: 22px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;快速开始</h>
            <br><br>
         
         - `什么是TransTrust <./docs/Introduction/index.md>`_
         - `系统设计 <./docs/Introduction/UseCases.md>`_
         - `使用场景 <./docs/Introduction/DesignDoc.md>`_
         - `开发样例 <./docs/Introduction/demo.md>`_


   .. container:: card-holder
      
      .. container:: card ref

         .. raw:: html

            <br>
            <h style="font-size: 22px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TransTrust组件</h>
            <br><br>
         
         - `SDK <./docs/Components/javasdk/index.md>`_
         - `数据服务 <./docs/Components/DataServer/index.md>`_
         - `datahub <./docs/Components/datahub/index.md>`_
         - `verifyDocment <./docs/Components/verifydocument/index.md>`_


.. toctree::
   :hidden:
   :maxdepth: 3
   :caption: 快速开始

   docs/Introduction/index.md
   docs/Introduction/UseCases.md
   docs/Introduction/DesignDoc.md
   docs/Introduction/demo.md
   docs/Introduction/Terminology

.. toctree::
   :hidden:
   :maxdepth: 3
   :caption: 核心模块设计解析

   docs/CoreConcepts/index.md
   docs/CoreConcepts/DID.md
   docs/CoreConcepts/CPT.md
   docs/CoreConcepts/Credential.md
   docs/CoreConcepts/Evidence.md
   docs/CoreConcepts/Transfer.md
   docs/CoreConcepts/Store.md

.. toctree::
   :hidden:
   :maxdepth: 4
   :caption: 基础组件
   
   docs/Components/javasdk/index.md
   docs/Components/DataServer/index.md
   docs/Components/verifydocument/index.md
   docs/Components/datahub/index.md
   docs/Components/Demo/index.md


.. toctree::
   :hidden:
   :maxdepth: 3
   :caption: 社区

   docs/community/CONTRIBUTING.md